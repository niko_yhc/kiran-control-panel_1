/**
 * Copyright (c) 2020 ~ 2023 KylinSec Co., Ltd.
 * kiran-control-panel is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 *
 * Author:     liuxinhao <liuxinhao@kylinsec.com.cn>
 */
#include "logging-category.h"

Q_LOGGING_CATEGORY(qLcDbusWrapper,"kcp.dbus-wrapper",QtMsgType::QtDebugMsg)
Q_LOGGING_CATEGORY(qLcCommonWidget,"kcp.common-widget",QtMsgType::QtDebugMsg)
Q_LOGGING_CATEGORY(qLcPluginFramework,"kcp.plugin-framework",QtMsgType::QtDebugMsg)

Q_LOGGING_CATEGORY(qLcAccount,"kcp.account",QtMsgType::QtDebugMsg)
Q_LOGGING_CATEGORY(qLcAppearance,"kcp.appearance",QtMsgType::QtDebugMsg)
Q_LOGGING_CATEGORY(qLcAudio,"kcp.audio",QtMsgType::QtDebugMsg)
Q_LOGGING_CATEGORY(qLcAuthentication,"kcp.authenticate",QtMsgType::QtDebugMsg)
Q_LOGGING_CATEGORY(qLcApplication,"kcp.application",QtMsgType::QtDebugMsg)
Q_LOGGING_CATEGORY(qLcDisplay,"kcp.display",QtMsgType::QtDebugMsg)
Q_LOGGING_CATEGORY(qLcGroup,"kcp.group",QtMsgType::QtDebugMsg)
Q_LOGGING_CATEGORY(qLcKeybinding,"kcp.keybinding",QtMsgType::QtDebugMsg)
Q_LOGGING_CATEGORY(qLcKeyboard,"kcp.keyboard",QtMsgType::QtDebugMsg)
Q_LOGGING_CATEGORY(qLcMouse,"kcp.mouse",QtMsgType::QtDebugMsg)
Q_LOGGING_CATEGORY(qLcNetwork,"kcp.network",QtMsgType::QtDebugMsg)
Q_LOGGING_CATEGORY(qLcPower,"kcp.power",QtMsgType::QtDebugMsg)
Q_LOGGING_CATEGORY(qLcSystem,"kcp.system",QtMsgType::QtDebugMsg)