/**
 * Copyright (c) 2020 ~ 2023 KylinSec Co., Ltd.
 * kiran-control-panel is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 *
 * Author:     liuxinhao <liuxinhao@kylinsec.com.cn>
 */
#pragma once

#include <QLoggingCategory>
#include <qt5-log-i.h>

Q_DECLARE_LOGGING_CATEGORY(qLcDbusWrapper)
Q_DECLARE_LOGGING_CATEGORY(qLcCommonWidget)
Q_DECLARE_LOGGING_CATEGORY(qLcPluginFramework)

Q_DECLARE_LOGGING_CATEGORY(qLcAccount)
Q_DECLARE_LOGGING_CATEGORY(qLcAppearance)
Q_DECLARE_LOGGING_CATEGORY(qLcAudio)
Q_DECLARE_LOGGING_CATEGORY(qLcAuthentication)
Q_DECLARE_LOGGING_CATEGORY(qLcApplication)
Q_DECLARE_LOGGING_CATEGORY(qLcDisplay)
Q_DECLARE_LOGGING_CATEGORY(qLcGroup)
Q_DECLARE_LOGGING_CATEGORY(qLcKeybinding)
Q_DECLARE_LOGGING_CATEGORY(qLcKeyboard)
Q_DECLARE_LOGGING_CATEGORY(qLcMouse)
Q_DECLARE_LOGGING_CATEGORY(qLcNetwork)
Q_DECLARE_LOGGING_CATEGORY(qLcPower)
Q_DECLARE_LOGGING_CATEGORY(qLcSystem)









