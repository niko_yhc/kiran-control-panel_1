<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AccountItemWidget</name>
    <message>
        <source>Create new user</source>
        <translation type="vanished">创建新用户</translation>
    </message>
    <message>
        <source>disable</source>
        <translation type="vanished">禁用</translation>
    </message>
    <message>
        <source>enable</source>
        <translation type="vanished">启用</translation>
    </message>
</context>
<context>
    <name>AccountSubItem</name>
    <message>
        <location filename="../plugins/account/account-subitem.cpp" line="36"/>
        <source>account</source>
        <translation>帐户</translation>
    </message>
    <message>
        <location filename="../plugins/account/account-subitem.cpp" line="62"/>
        <source>New User</source>
        <translation>创建新用户</translation>
    </message>
</context>
<context>
    <name>AccountWidget</name>
    <message>
        <location filename="../plugins/account/account-widget.cpp" line="100"/>
        <location filename="../plugins/account/account-widget.cpp" line="420"/>
        <source>disable</source>
        <translation type="unfinished">禁用</translation>
    </message>
    <message>
        <location filename="../plugins/account/account-widget.cpp" line="100"/>
        <location filename="../plugins/account/account-widget.cpp" line="420"/>
        <source>enable</source>
        <translation type="unfinished">启用</translation>
    </message>
    <message>
        <location filename="../plugins/account/account-widget.cpp" line="225"/>
        <source>Create new user</source>
        <translation type="unfinished">创建新用户</translation>
    </message>
</context>
<context>
    <name>ActGuideWidget</name>
    <message>
        <source>Please choose activation mode:</source>
        <translation type="vanished">请选择激活方式：</translation>
    </message>
    <message>
        <source>Next</source>
        <translation type="vanished">下一步</translation>
    </message>
    <message>
        <source>Current machine code:</source>
        <translation type="vanished">当前系统机器码:</translation>
    </message>
    <message>
        <source>Please input activation code:</source>
        <translation type="vanished">请输入激活码:</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">上一步</translation>
    </message>
    <message>
        <source>Please insert the UsbKey device first!</source>
        <translation type="vanished">请先插入UsbKey设备!</translation>
    </message>
    <message>
        <source>Activate online</source>
        <translation type="vanished">在线激活</translation>
    </message>
    <message>
        <source>Activate with  the input activation code</source>
        <translation type="vanished">输入激活码激活</translation>
    </message>
    <message>
        <source>Activate with insert UsbKey</source>
        <translation type="vanished">插入UsbKey设备激活</translation>
    </message>
    <message>
        <source>Activate</source>
        <translation type="vanished">激活</translation>
    </message>
    <message>
        <source>Please enter the activate server address:</source>
        <translation type="vanished">请输入激活服务器地址:</translation>
    </message>
    <message>
        <source>Activating......</source>
        <translation type="vanished">正在激活......</translation>
    </message>
    <message>
        <source>System activate successful!</source>
        <translation type="vanished">系统激活成功!</translation>
    </message>
    <message>
        <source>server activation and remaining points:</source>
        <translation type="vanished">服务器版激活点数和剩余点数:</translation>
    </message>
    <message>
        <source>development activation and remaining points:</source>
        <translation type="vanished">开发版激活点数和剩余点数:</translation>
    </message>
    <message>
        <source>industrial activation and remaining points:</source>
        <translation type="vanished">工控版激活点数和剩余点数:</translation>
    </message>
    <message>
        <source>desktop activation and remaining points:</source>
        <translation type="vanished">桌面版激活点数和剩余点数:</translation>
    </message>
    <message>
        <source>activation points:</source>
        <translation type="vanished">激活点数:</translation>
    </message>
    <message>
        <source>remaining points:</source>
        <translation type="vanished">剩余点数:</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>System activate failed!</source>
        <translation type="vanished">系统激活失败!</translation>
    </message>
    <message>
        <source>unknow</source>
        <translation type="vanished">未知</translation>
    </message>
    <message>
        <source>Activation Mode</source>
        <translation type="vanished">选择激活方式</translation>
    </message>
    <message>
        <source>Start Activation</source>
        <translation type="vanished">开始激活</translation>
    </message>
    <message>
        <source>Activation Complete</source>
        <translation type="vanished">激活完成</translation>
    </message>
    <message>
        <source>Activation Guide</source>
        <translation type="vanished">激活向导</translation>
    </message>
    <message>
        <source>Server IP address or Domain name</source>
        <translation type="vanished">服务器IP地址或域名</translation>
    </message>
</context>
<context>
    <name>AdvanceSettings</name>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.cpp" line="133"/>
        <source>Advance Settings</source>
        <translation>高级设置</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.cpp" line="151"/>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.cpp" line="161"/>
        <source>Automatically generated by system</source>
        <translation>由系统自动生成</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.cpp" line="192"/>
        <source>Please enter the correct path</source>
        <translation>请输入正确的路径</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.cpp" line="201"/>
        <source>Please enter specify user Id</source>
        <translation>请输入用户ID</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.cpp" line="207"/>
        <source>Please enter an integer above 1000</source>
        <translation>请输入一个大于或等于1000的整数</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.cpp" line="216"/>
        <source>Please enter the correct home directory</source>
        <translation>请输入正确的用户目录</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.ui" line="61"/>
        <source>Login shell</source>
        <translation>登录Shell</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.ui" line="93"/>
        <source>EditLoginShell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.ui" line="110"/>
        <source>Specify user id (needs to be greater than 1000)</source>
        <translation>指定用户ID（需大于或等于1000）</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.ui" line="165"/>
        <source>EditSpecifyUserID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.ui" line="183"/>
        <source>Specify user home</source>
        <translation>指定用户目录</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.ui" line="228"/>
        <source>EditSpecifyUserHome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.ui" line="289"/>
        <source>ButtonConfirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.ui" line="292"/>
        <source>confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.ui" line="333"/>
        <source>ButtonCancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/advance-settings-page/advance-settings.ui" line="336"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Specify user id</source>
        <translation type="vanished">指定用户ID</translation>
    </message>
</context>
<context>
    <name>AppearancePlugin</name>
    <message>
        <location filename="../plugins/appearance/appearance-plugin.cpp" line="67"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/appearance-plugin.cpp" line="74"/>
        <source>Wallpaper</source>
        <translation>壁纸</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/appearance-plugin.cpp" line="81"/>
        <source>Font</source>
        <translation>字体</translation>
    </message>
</context>
<context>
    <name>ApplicationPlugin</name>
    <message>
        <location filename="../plugins/application/application-plugin.cpp" line="71"/>
        <source>DefaultApp</source>
        <translation>默认程序</translation>
    </message>
    <message>
        <location filename="../plugins/application/application-plugin.cpp" line="78"/>
        <source>AutoStart</source>
        <translation>开机启动</translation>
    </message>
</context>
<context>
    <name>AudioSystemTray</name>
    <message>
        <location filename="../plugins/audio/src/system-tray/audio-system-tray.cpp" line="93"/>
        <source>Volume Setting</source>
        <translation>声音设置</translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/system-tray/audio-system-tray.cpp" line="94"/>
        <source>Mixed Setting</source>
        <translation>混合设置</translation>
    </message>
</context>
<context>
    <name>AuthManagerPage</name>
    <message>
        <source>Fingerprint Authentication</source>
        <translation type="vanished">指纹认证</translation>
    </message>
    <message>
        <source>Face Authentication</source>
        <translation type="vanished">人脸认证</translation>
    </message>
    <message>
        <source>Password Authentication</source>
        <translation type="vanished">密码认证</translation>
    </message>
    <message>
        <source>save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>return</source>
        <translation type="vanished">返回</translation>
    </message>
    <message>
        <source>add fingerprint</source>
        <translation type="vanished">录入指纹数据</translation>
    </message>
    <message>
        <source>add face</source>
        <translation type="vanished">录入人脸数据</translation>
    </message>
    <message>
        <source>error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>please ensure that at least one authentication option exists</source>
        <translation type="vanished">请确保至少一个认证选项打开</translation>
    </message>
    <message>
        <source>fingerprint_</source>
        <translation type="vanished">指纹_</translation>
    </message>
    <message>
        <source>face_</source>
        <translation type="vanished">人脸_</translation>
    </message>
</context>
<context>
    <name>AuthPlugin</name>
    <message>
        <location filename="../plugins/authentication/auth-plugin.cpp" line="91"/>
        <source>Fingerprint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/auth-plugin.cpp" line="98"/>
        <source>FingerVein</source>
        <translation>指静脉</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/auth-plugin.cpp" line="126"/>
        <source>Driver Manager</source>
        <translation>驱动管理</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/auth-plugin.cpp" line="133"/>
        <source>Prefs</source>
        <translation>配置</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/auth-plugin.cpp" line="105"/>
        <source>UKey</source>
        <translation>UKey</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/auth-plugin.cpp" line="112"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/auth-plugin.cpp" line="119"/>
        <source>Face</source>
        <translation>人脸</translation>
    </message>
</context>
<context>
    <name>AutoStartPage</name>
    <message>
        <source>Boot Setup</source>
        <translation type="vanished">开机启动设置</translation>
    </message>
    <message>
        <source>Desktop files(*.desktop)</source>
        <translation type="vanished">桌面类型(*.desktop)</translation>
    </message>
    <message>
        <source>select autostart desktop</source>
        <translation type="vanished">选择自启动应用</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">选择</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>Desktop has existed</source>
        <translation type="vanished">该程序已存在</translation>
    </message>
    <message>
        <source>Desktop cant permit to join</source>
        <translation type="vanished">该程序不允许添加</translation>
    </message>
    <message>
        <source>Desktop dont support</source>
        <translation type="vanished">不支持该程序</translation>
    </message>
</context>
<context>
    <name>AutostartPage</name>
    <message>
        <location filename="../plugins/application/autostart/autostart-page.cpp" line="96"/>
        <source>Boot Setup</source>
        <translation>开机启动设置</translation>
    </message>
    <message>
        <location filename="../plugins/application/autostart/autostart-page.cpp" line="231"/>
        <source>Desktop files(*.desktop)</source>
        <translation>桌面类型(*.desktop)</translation>
    </message>
    <message>
        <location filename="../plugins/application/autostart/autostart-page.cpp" line="239"/>
        <source>select autostart desktop</source>
        <translation>选择自启动应用</translation>
    </message>
    <message>
        <location filename="../plugins/application/autostart/autostart-page.cpp" line="240"/>
        <source>Select</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="../plugins/application/autostart/autostart-page.cpp" line="241"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../plugins/application/autostart/autostart-page.cpp" line="256"/>
        <location filename="../plugins/application/autostart/autostart-page.cpp" line="261"/>
        <location filename="../plugins/application/autostart/autostart-page.cpp" line="266"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../plugins/application/autostart/autostart-page.cpp" line="256"/>
        <source>Desktop has existed</source>
        <translation>该程序已存在</translation>
    </message>
    <message>
        <location filename="../plugins/application/autostart/autostart-page.cpp" line="261"/>
        <source>Desktop cant permit to join</source>
        <translation>该程序不允许添加</translation>
    </message>
    <message>
        <location filename="../plugins/application/autostart/autostart-page.cpp" line="266"/>
        <source>Desktop dont support</source>
        <translation>不支持该程序</translation>
    </message>
</context>
<context>
    <name>BatterySettingsPage</name>
    <message>
        <location filename="../plugins/power/pages/battery-settings-page.ui" line="14"/>
        <source>BatterySettingsPage</source>
        <translation>电池设置</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/battery-settings-page.ui" line="43"/>
        <source>After idle for more than the following time, the computer will execute</source>
        <translation>空闲超过以下时间后，计算机将执行</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/battery-settings-page.ui" line="60"/>
        <source>ComboIdleTime</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/battery-settings-page.ui" line="67"/>
        <source>ComboIdleAction</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/battery-settings-page.ui" line="87"/>
        <source>When the battery is lit up, it will be executed</source>
        <translation>电池电量将用尽时</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/battery-settings-page.ui" line="104"/>
        <source>ComboLowBatteryAction</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/battery-settings-page.ui" line="121"/>
        <source>The monitor will turn off when it is idle</source>
        <translation>显示器空闲以下时间关闭</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/battery-settings-page.ui" line="138"/>
        <source>ComboMonitorTurnOffIdleTime</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/battery-settings-page.ui" line="155"/>
        <source>Reduce screen brightness when idle</source>
        <translation>空闲时减少亮度</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/battery-settings-page.ui" line="182"/>
        <source>Reduce screen brightness when  no power</source>
        <translation>低点亮时减少亮度</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/battery-settings-page.ui" line="212"/>
        <source>The energy saving mode is enabled when the power is low</source>
        <translation>低电量时自动开启节能模式</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/battery-settings-page.cpp" line="59"/>
        <location filename="../plugins/power/pages/battery-settings-page.cpp" line="75"/>
        <source>Suspend</source>
        <translation>待机</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/battery-settings-page.cpp" line="60"/>
        <location filename="../plugins/power/pages/battery-settings-page.cpp" line="76"/>
        <source>Shutdown</source>
        <translation>关机</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/battery-settings-page.cpp" line="61"/>
        <location filename="../plugins/power/pages/battery-settings-page.cpp" line="77"/>
        <source>Hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/battery-settings-page.cpp" line="62"/>
        <location filename="../plugins/power/pages/battery-settings-page.cpp" line="78"/>
        <source>Do nothing</source>
        <translation>不执行操作</translation>
    </message>
</context>
<context>
    <name>BatterySubItem</name>
    <message>
        <source>Battery Settings</source>
        <translation type="vanished">电池设置</translation>
    </message>
</context>
<context>
    <name>BiometricItem</name>
    <message>
        <source>add</source>
        <translation type="vanished">添加</translation>
    </message>
</context>
<context>
    <name>CPanelAudioWidget</name>
    <message>
        <location filename="../plugins/audio/src/plugin/cpanel-audio-widget.ui" line="14"/>
        <source>CPanelAudioWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/cpanel-audio-widget.cpp" line="40"/>
        <source>Output</source>
        <translation>输出</translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/cpanel-audio-widget.cpp" line="41"/>
        <source>Input</source>
        <translation>输入</translation>
    </message>
</context>
<context>
    <name>CPanelNetworkWidget</name>
    <message>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.ui" line="20"/>
        <source>CPanelNetworkWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="71"/>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="74"/>
        <source>VPN</source>
        <translation>VPN</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="80"/>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="83"/>
        <source>Network Details</source>
        <translation>网络详情</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="124"/>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="125"/>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="237"/>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="338"/>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="381"/>
        <source>Wired Network</source>
        <translation>有线网络</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="157"/>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="158"/>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="225"/>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="312"/>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="345"/>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="393"/>
        <source>Wireless Network</source>
        <translation>无线网络</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="270"/>
        <source>Connected</source>
        <translation>已连接</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="274"/>
        <source>Unavailable</source>
        <translation>已禁用</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/cpanel-network-widget.cpp" line="278"/>
        <source>Disconnected</source>
        <translation>已断开</translation>
    </message>
    <message>
        <source>Wired Network %1</source>
        <translation type="obsolete">有线网络 %1</translation>
    </message>
    <message>
        <source>Wireless Network %1</source>
        <translation type="obsolete">无线网络  %1</translation>
    </message>
</context>
<context>
    <name>ChangeHostNameWidget</name>
    <message>
        <location filename="../plugins/system/pages/system-information/change-host-name-widget.ui" line="32"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/change-host-name-widget.ui" line="107"/>
        <source>Host Name:</source>
        <translation>主机名:</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/change-host-name-widget.ui" line="132"/>
        <source>EditHostName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/change-host-name-widget.ui" line="185"/>
        <source>ButtonSaveHostName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/change-host-name-widget.ui" line="188"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/change-host-name-widget.ui" line="223"/>
        <source>ButtonCancelChangeHostName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/change-host-name-widget.ui" line="226"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Host Name</source>
        <translation type="vanished">主机名</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/change-host-name-widget.cpp" line="72"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/change-host-name-widget.cpp" line="73"/>
        <source>Change host name failed! Please check the Dbus service!</source>
        <translation>修改主机名失败！请 检查Dbus服务!</translation>
    </message>
</context>
<context>
    <name>CheckpasswdDialog</name>
    <message>
        <location filename="../plugins/authentication/checkpasswd-dialog.cpp" line="96"/>
        <source>Check password</source>
        <translation>校验当前用户密码</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/checkpasswd-dialog.cpp" line="97"/>
        <source>Check the current password before you enroll the feature</source>
        <translation>录入特征之前需要校验当前密码</translation>
    </message>
</context>
<context>
    <name>ChooseItem</name>
    <message>
        <location filename="../plugins/keyboard/utils/choose-item.ui" line="35"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ConnectionDetailsWidget</name>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="14"/>
        <source>ConnectionDetailsWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="89"/>
        <source>Security type</source>
        <translation>安全类型</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="109"/>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="175"/>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="241"/>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="315"/>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="381"/>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="447"/>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="513"/>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="579"/>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="645"/>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="711"/>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="777"/>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="843"/>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="909"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="155"/>
        <source>Frequency band</source>
        <translation>频段</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="221"/>
        <source>Channel</source>
        <translation>网络通道</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="295"/>
        <source>Interface</source>
        <translation>接口</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="361"/>
        <source>MAC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="427"/>
        <source>IPv4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="493"/>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="757"/>
        <source>Gateway</source>
        <translation>网关</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="559"/>
        <source>DNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="625"/>
        <source>Subnet mask</source>
        <translation>子网掩码</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="691"/>
        <source>IPv6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="823"/>
        <source>Prefix</source>
        <translation>前缀</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/connection-details-widget.ui" line="889"/>
        <source>Rate</source>
        <translation>速率</translation>
    </message>
    <message>
        <source>Preferred DNS</source>
        <translation type="vanished">首选DNS</translation>
    </message>
</context>
<context>
    <name>ConnectionItemWidget</name>
    <message>
        <location filename="../plugins/network/src/plugin/connection-itemwidget.cpp" line="79"/>
        <source>disconnect</source>
        <translation>断开</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/connection-itemwidget.cpp" line="120"/>
        <source>ignore</source>
        <translation>忽略</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/connection-itemwidget.cpp" line="126"/>
        <source>remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/connection-itemwidget.cpp" line="292"/>
        <source>The current device:%1 is not available</source>
        <translation>当前设备：%1 不可用</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/connection-itemwidget.cpp" line="308"/>
        <source>The carrier is pulled out</source>
        <translation>网线被拔出</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/connection-itemwidget.cpp" line="396"/>
        <source>Are you sure you want to delete the connection %1</source>
        <translation>您是否确定要删除连接 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/connection-itemwidget.cpp" line="397"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/connection-itemwidget.cpp" line="454"/>
        <location filename="../plugins/network/src/plugin/connection-itemwidget.cpp" line="467"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/connection-itemwidget.cpp" line="455"/>
        <source>Password required to connect to %1.</source>
        <translation>连接网络 &quot;%1&quot; 需要密码</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/connection-itemwidget.cpp" line="468"/>
        <source>Please input a network name</source>
        <translation>请输入网络名称</translation>
    </message>
</context>
<context>
    <name>ConnectionNameWidget</name>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/connection-name-widget.ui" line="14"/>
        <source>ConnectionNameWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/connection-name-widget.ui" line="58"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/connection-name-widget.ui" line="77"/>
        <source>EditConnectionName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/connection-name-widget.ui" line="88"/>
        <source>Auto Connection</source>
        <translation>自动连接</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/connection-name-widget.cpp" line="44"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/connection-name-widget.cpp" line="85"/>
        <source>Wired Connection %1</source>
        <translation>有线网络%1</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/connection-name-widget.cpp" line="124"/>
        <source>VPN L2TP %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/connection-name-widget.cpp" line="127"/>
        <source>VPN PPTP %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/connection-name-widget.cpp" line="188"/>
        <source>Connection name can not be empty</source>
        <translation>网络名称不能为空</translation>
    </message>
</context>
<context>
    <name>ConnectionShowPage</name>
    <message>
        <location filename="../plugins/network/src/plugin/connection-show-page.ui" line="14"/>
        <source>ConnectionShowPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/connection-show-page.ui" line="62"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/connection-show-page.ui" line="93"/>
        <source>ButtonCreateConnection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateGroupPage</name>
    <message>
        <location filename="../plugins/group/src/pages/create-group-page/create-group-page.ui" line="32"/>
        <source>CreateGroupPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/create-group-page/create-group-page.ui" line="95"/>
        <source>Create Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/create-group-page/create-group-page.ui" line="166"/>
        <source>Add Group Members</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/create-group-page/create-group-page.ui" line="246"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/create-group-page/create-group-page.cpp" line="101"/>
        <source>Please enter your group name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/create-group-page/create-group-page.cpp" line="118"/>
        <source>group name cannot be a pure number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/create-group-page/create-group-page.cpp" line="125"/>
        <source>group name already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/create-group-page/create-group-page.cpp" line="153"/>
        <location filename="../plugins/group/src/pages/create-group-page/create-group-page.cpp" line="172"/>
        <source>Error</source>
        <translation type="unfinished">错误</translation>
    </message>
</context>
<context>
    <name>CreateUserPage</name>
    <message>
        <source>Account type</source>
        <translation type="vanished">帐户类型</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.cpp" line="83"/>
        <source>standard</source>
        <translation>普通用户</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.cpp" line="84"/>
        <source>administrator</source>
        <translation>管理员</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.cpp" line="107"/>
        <source>Please enter user name first</source>
        <translation>请输入用户名</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.cpp" line="133"/>
        <source>Please enter your user name</source>
        <translation>请输入用户名</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.cpp" line="149"/>
        <source>user name cannot be a pure number</source>
        <translation>用户名不能全为数字</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.cpp" line="156"/>
        <source>user name already exists</source>
        <translation>用户名已存在</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.cpp" line="168"/>
        <source>Please enter your password</source>
        <translation>请输出密码</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.cpp" line="175"/>
        <source>Please enter the password again</source>
        <translation>请再次输入密码</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.cpp" line="182"/>
        <source>The password you enter must be the same as the former one</source>
        <translation>两次密码不相同，请核对后，再次输入</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.cpp" line="192"/>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.cpp" line="227"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.cpp" line="192"/>
        <source>Password encryption failed</source>
        <translation>密码加密失败</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.ui" line="74"/>
        <source>UserAvatarWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.ui" line="104"/>
        <source>User name</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.ui" line="136"/>
        <source>EditUserName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.ui" line="153"/>
        <source>User type</source>
        <translation>用户类型</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.ui" line="182"/>
        <source>ComboUserType</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.ui" line="199"/>
        <source>Password</source>
        <translation>用户密码</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.ui" line="234"/>
        <source>EditPasswd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.ui" line="251"/>
        <source>Confirm password</source>
        <translation>确认密码</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.ui" line="286"/>
        <source>EditPasswdConfirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.ui" line="306"/>
        <source>ButtonAdvanceSetting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.ui" line="312"/>
        <source>Advance setting</source>
        <translation>高级设置</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.ui" line="407"/>
        <source>ButtonConfirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.ui" line="410"/>
        <source>Confirm</source>
        <translation>创建</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.ui" line="457"/>
        <source>ButtonCancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/create-user-page/create-user-page.ui" line="460"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Please enter account name first</source>
        <translation type="vanished">请先输入帐户名</translation>
    </message>
    <message>
        <source>Please enter your account name</source>
        <translation type="vanished">请输入帐户名</translation>
    </message>
    <message>
        <source>Account cannot be a pure number</source>
        <translation type="vanished">帐户名不能全为数字</translation>
    </message>
    <message>
        <source>Account already exists</source>
        <translation type="vanished">帐户名已存在</translation>
    </message>
    <message>
        <source>Please enter your userName name</source>
        <translation type="vanished">请输入用户名</translation>
    </message>
</context>
<context>
    <name>CursorThemePage</name>
    <message>
        <location filename="../plugins/appearance/pages/theme/cursor/cursor-theme-page.cpp" line="55"/>
        <source>Cursor Themes Settings</source>
        <translation type="unfinished">光标主题设置</translation>
    </message>
</context>
<context>
    <name>CursorThemes</name>
    <message>
        <source>Cursor Themes Settings</source>
        <translation type="vanished">光标主题设置</translation>
    </message>
    <message>
        <source>Faild</source>
        <translation type="vanished">失败</translation>
    </message>
    <message>
        <source>Set cursor themes failed!</source>
        <translation type="vanished">设置光标主题失败!</translation>
    </message>
</context>
<context>
    <name>DateTimeSettings</name>
    <message>
        <location filename="../plugins/timedate/pages/date-time-settings/date-time-settings.ui" line="14"/>
        <source>DateTimeSettings</source>
        <translation>日期时间设置</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/date-time-settings/date-time-settings.ui" line="46"/>
        <source>Select Time</source>
        <translation>选择时间</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/date-time-settings/date-time-settings.ui" line="88"/>
        <source>Select Date</source>
        <translation>选择日期</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/date-time-settings/date-time-settings.ui" line="152"/>
        <source>ButtonSave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/date-time-settings/date-time-settings.ui" line="155"/>
        <source>save</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/date-time-settings/date-time-settings.ui" line="196"/>
        <source>ButtonReset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/date-time-settings/date-time-settings.ui" line="199"/>
        <source>reset</source>
        <translation>重置</translation>
    </message>
</context>
<context>
    <name>DaySpinBox</name>
    <message>
        <location filename="../plugins/timedate/widgets/date-spinbox.h" line="65"/>
        <source>%1</source>
        <translation>%1日</translation>
    </message>
</context>
<context>
    <name>DefaultApp</name>
    <message>
        <location filename="../plugins/application/defaultapp/defaultapp.ui" line="14"/>
        <source>DefaultApp</source>
        <translation>默认程序</translation>
    </message>
    <message>
        <location filename="../plugins/application/defaultapp/defaultapp.ui" line="50"/>
        <source>Web Browser</source>
        <translation>web浏览器</translation>
    </message>
    <message>
        <location filename="../plugins/application/defaultapp/defaultapp.ui" line="84"/>
        <source>Email</source>
        <translation>电子邮件</translation>
    </message>
    <message>
        <location filename="../plugins/application/defaultapp/defaultapp.ui" line="115"/>
        <source>Text</source>
        <translation>文档查看器</translation>
    </message>
    <message>
        <location filename="../plugins/application/defaultapp/defaultapp.ui" line="146"/>
        <source>Music</source>
        <translation>音乐播放器</translation>
    </message>
    <message>
        <location filename="../plugins/application/defaultapp/defaultapp.ui" line="177"/>
        <source>Video</source>
        <translation>视频播放器</translation>
    </message>
    <message>
        <location filename="../plugins/application/defaultapp/defaultapp.ui" line="208"/>
        <source>Image</source>
        <translation>图片查看器</translation>
    </message>
</context>
<context>
    <name>DefaultappPlugin</name>
    <message>
        <source>Email</source>
        <translation type="obsolete">电子邮件</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="obsolete">文档查看器</translation>
    </message>
    <message>
        <source>Music</source>
        <translation type="obsolete">音乐播放器</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="obsolete">视频播放器</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="obsolete">图片查看器</translation>
    </message>
    <message>
        <source>DefaultApp</source>
        <translation type="vanished">默认程序</translation>
    </message>
</context>
<context>
    <name>DetailsPage</name>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/details-page.ui" line="14"/>
        <source>DetailsPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/details-page.ui" line="62"/>
        <source>Network Details</source>
        <translation>网络详情</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/details-page.ui" line="121"/>
        <source>Please select a connection</source>
        <translation>请选择连接</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/details-page/details-page.ui" line="140"/>
        <source>ComboBoxDetailsSelectConnection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeviceAvailableConnectionWidget</name>
    <message>
        <location filename="../plugins/network/src/plugin/device-available-connection-widget.cpp" line="281"/>
        <source>Network card: %1</source>
        <translation>网卡：%1</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/device-available-connection-widget.cpp" line="408"/>
        <source>Other WiFi networks</source>
        <translation>其它WIFI网络</translation>
    </message>
</context>
<context>
    <name>DeviceList</name>
    <message>
        <location filename="../plugins/network/src/plugin/device-list.cpp" line="154"/>
        <source>Wired Network Adapter</source>
        <translation>有线网络配置</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/device-list.cpp" line="158"/>
        <source>Wireless Network Adapter</source>
        <translation>无线网络配置</translation>
    </message>
</context>
<context>
    <name>DevicePanel</name>
    <message>
        <location filename="../plugins/display/src/device-panel.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/device-panel.ui" line="122"/>
        <source>Rotate left 90 degrees</source>
        <translation>左旋转90度</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/device-panel.ui" line="125"/>
        <source>ButtonLeft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/device-panel.ui" line="156"/>
        <source>Rotate right 90 degrees</source>
        <translation>右旋转90度</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/device-panel.ui" line="159"/>
        <source>ButtonRight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/device-panel.ui" line="190"/>
        <source>Turn left and right</source>
        <translation>左右翻转</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/device-panel.ui" line="193"/>
        <source>ButtonHorizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/device-panel.ui" line="227"/>
        <source>upside down</source>
        <translation>上下翻转</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/device-panel.ui" line="230"/>
        <source>ButtonVertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/device-panel.ui" line="264"/>
        <source>Identification display</source>
        <translation>标识显示器</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/device-panel.ui" line="267"/>
        <source>ButtonIdentifying</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DisconnectAndDeleteButton</name>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.ui" line="14"/>
        <source>DisconnectAndDeleteButton</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.ui" line="35"/>
        <source>ButtonDisconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.ui" line="38"/>
        <source>Disconnect</source>
        <translation>断开</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.ui" line="45"/>
        <source>ButtonDelete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.ui" line="48"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.ui" line="55"/>
        <source>ButtonIgnore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.ui" line="58"/>
        <source>Ignore</source>
        <translation>忽略</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.cpp" line="101"/>
        <source>Are you sure you want to delete the connection %1</source>
        <translation>您是否确定要删除连接 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/disconnect-and-delete-button.cpp" line="102"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
</context>
<context>
    <name>DisplayFormatSettings</name>
    <message>
        <location filename="../plugins/timedate/pages/display-format-settings/display-format-settings.ui" line="14"/>
        <source>DisplayFormatSettings</source>
        <translation>日期时间格式设置</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/display-format-settings/display-format-settings.ui" line="46"/>
        <source>Long date display format</source>
        <translation>长日期显示格式</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/display-format-settings/display-format-settings.ui" line="55"/>
        <source>ComboLongDateDisplayFormat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/display-format-settings/display-format-settings.ui" line="73"/>
        <source>Short date display format</source>
        <translation>短日期显示格式</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/display-format-settings/display-format-settings.ui" line="82"/>
        <source>ComboShortDateDisplayFormat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/display-format-settings/display-format-settings.ui" line="97"/>
        <source>Time format</source>
        <translation>时间格式</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/display-format-settings/display-format-settings.ui" line="106"/>
        <source>ComboTimeFormat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/display-format-settings/display-format-settings.ui" line="124"/>
        <source>Show time witch seconds</source>
        <translation>时间显示秒</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/display-format-settings/display-format-settings.cpp" line="64"/>
        <source>24-hours</source>
        <translation>二十四小时制</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/display-format-settings/display-format-settings.cpp" line="65"/>
        <source>12-hours</source>
        <translation>十二小时制</translation>
    </message>
</context>
<context>
    <name>DisplayPage</name>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="14"/>
        <source>DisplayPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="110"/>
        <source>ButtonCopyDisplay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="134"/>
        <source>Copy display</source>
        <translation>复制显示</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="162"/>
        <source>ButtonExtendedDisplay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="186"/>
        <source>Extended display</source>
        <translation>扩展显示</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="289"/>
        <location filename="../plugins/display/src/display-page.ui" line="562"/>
        <source>Resolution ratio</source>
        <translation>分辨率</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="308"/>
        <source>ComboResolutionRatio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="325"/>
        <location filename="../plugins/display/src/display-page.ui" line="598"/>
        <source>Refresh rate</source>
        <translation>刷新率</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="344"/>
        <source>ComboRefreshRate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="358"/>
        <location filename="../plugins/display/src/display-page.ui" line="631"/>
        <source>Zoom rate</source>
        <translation>缩放率</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="377"/>
        <source>ComboZoomRate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="381"/>
        <location filename="../plugins/display/src/display-page.ui" line="654"/>
        <source>Automatic</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="386"/>
        <location filename="../plugins/display/src/display-page.ui" line="659"/>
        <source>100% (recommended)</source>
        <translation>100% (推荐)</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="391"/>
        <location filename="../plugins/display/src/display-page.ui" line="664"/>
        <source>200%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="462"/>
        <source>Open</source>
        <translation>开启</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="514"/>
        <source>Set as main display</source>
        <translation>设为主显示器</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="540"/>
        <source>SwitchExtraPrimary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="581"/>
        <source>ComboExtraResolutionRatio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="617"/>
        <source>ComboExtraRefreshRate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="650"/>
        <source>ComboExtraZoomRate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="731"/>
        <source>ButtonExtraApply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="737"/>
        <source>Apply</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="756"/>
        <source>ButtonExtraCancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.ui" line="762"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.cpp" line="255"/>
        <location filename="../plugins/display/src/display-page.cpp" line="273"/>
        <source> (recommended)</source>
        <translation> (推荐)</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.cpp" line="364"/>
        <source>Is the display normal?</source>
        <translation>显示是否正常?</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.cpp" line="367"/>
        <source>Save current configuration(K)</source>
        <translation>保存当前配置(K)</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.cpp" line="371"/>
        <source>Restore previous configuration(R)</source>
        <translation>恢复之前的配置(R)</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.cpp" line="379"/>
        <source>The display will resume the previous configuration in %1 seconds</source>
        <translation>显示将会在 %1 秒后恢复之前的配置</translation>
    </message>
</context>
<context>
    <name>DisplaySubitem</name>
    <message>
        <location filename="../plugins/display/src/display-subitem.h" line="29"/>
        <source>Display</source>
        <translation>显示</translation>
    </message>
</context>
<context>
    <name>DnsWidget</name>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/dns-widget.ui" line="14"/>
        <source>DnsWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/dns-widget.ui" line="32"/>
        <source>Preferred DNS</source>
        <translation>首选DNS</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/dns-widget.ui" line="42"/>
        <source>Alternate DNS</source>
        <translation>备选DNS</translation>
    </message>
</context>
<context>
    <name>DriverPage</name>
    <message>
        <location filename="../plugins/authentication/pages/driver-page.cpp" line="43"/>
        <source>device type</source>
        <translation>设备类型</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/driver-page.cpp" line="55"/>
        <source>driver list</source>
        <translation>驱动列表</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/driver-page.cpp" line="66"/>
        <source>Fingerprint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="vanished">指静脉</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/driver-page.cpp" line="67"/>
        <source>Fingervein</source>
        <translation>指静脉</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/driver-page.cpp" line="68"/>
        <source>iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/driver-page.cpp" line="69"/>
        <source>ukey</source>
        <translation>UKey</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/driver-page.cpp" line="70"/>
        <source>face</source>
        <translation>人脸</translation>
    </message>
</context>
<context>
    <name>DslManager</name>
    <message>
        <location filename="../plugins/network/src/plugin/manager/dsl-manager.ui" line="14"/>
        <source>DslManager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/manager/dsl-manager.cpp" line="33"/>
        <source>DSL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DslSettingPage</name>
    <message>
        <location filename="../plugins/network/src/plugin/settings/dsl-setting-page.ui" line="14"/>
        <source>DslSettingPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/settings/dsl-setting-page.ui" line="72"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/settings/dsl-setting-page.ui" line="92"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
</context>
<context>
    <name>EthernetWidget</name>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ethernet-widget.ui" line="14"/>
        <source>EthernetWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ethernet-widget.ui" line="40"/>
        <source>MAC Address Of Ethernet Device</source>
        <translation>设备MAC地址</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ethernet-widget.ui" line="59"/>
        <source>ComboBoxDeviceMac</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ethernet-widget.ui" line="76"/>
        <source>Ethernet Clone MAC Address</source>
        <translation>克隆MAC地址</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ethernet-widget.ui" line="95"/>
        <source>EditDeviceMac</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ethernet-widget.ui" line="117"/>
        <source>Custom MTU</source>
        <translation>自定义MTU</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ethernet-widget.ui" line="151"/>
        <source>SpinBoxCustomMTU</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ethernet-widget.cpp" line="46"/>
        <source>No device specified</source>
        <translation>不指定设备</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ethernet-widget.cpp" line="168"/>
        <source>Clone Mac invalid</source>
        <translation>无效的克隆MAC地址</translation>
    </message>
</context>
<context>
    <name>FaceEnrollDialog</name>
    <message>
        <source>save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>initializing face collection environment...</source>
        <translation type="vanished">正在初始化人脸采集环境，请稍后</translation>
    </message>
    <message>
        <source>failed to initialize face collection environment!</source>
        <translation type="vanished">初始化人脸采集环境失败!</translation>
    </message>
    <message>
        <source>Failed to start collection</source>
        <translation type="vanished">开始采集失败</translation>
    </message>
</context>
<context>
    <name>FaceInputDialog</name>
    <message>
        <source>save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>initializing face collection environment...</source>
        <translation type="vanished">正在初始化人脸采集环境，请稍后</translation>
    </message>
    <message>
        <source>failed to initialize face collection environment!</source>
        <translation type="vanished">初始化人脸采集环境失败!</translation>
    </message>
    <message>
        <source>Failed to start collection</source>
        <translation type="vanished">开始采集失败</translation>
    </message>
</context>
<context>
    <name>FacePage</name>
    <message>
        <location filename="../plugins/authentication/pages/face-page.cpp" line="66"/>
        <source>Default face device</source>
        <translation>默认人脸设备</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/face-page.cpp" line="67"/>
        <source>face feature list</source>
        <translation>人脸特征列表</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/face-page.cpp" line="65"/>
        <source>face</source>
        <translation>人脸</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/face-page.cpp" line="99"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/face-page.cpp" line="147"/>
        <source>Start enroll failed,%1</source>
        <translation>开始录入失败,%1</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/face-page.cpp" line="148"/>
        <location filename="../plugins/authentication/pages/face-page.cpp" line="179"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/face-page.cpp" line="173"/>
        <source>The biometric features were successfully recorded. The feature name is:%1</source>
        <translation>特征已成功录入，特征名为:%1</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/face-page.cpp" line="174"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/face-page.cpp" line="178"/>
        <source>Failed to record biometrics(%1), Please try again</source>
        <translation>录入特征失败(%1),请重试</translation>
    </message>
</context>
<context>
    <name>FingerPage</name>
    <message>
        <location filename="../plugins/authentication/pages/finger-page.cpp" line="122"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/finger-page.cpp" line="88"/>
        <source>fingerprint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/finger-page.cpp" line="88"/>
        <source>fingervein</source>
        <translation>指静脉</translation>
    </message>
    <message>
        <source>default %1 device</source>
        <translation type="vanished">默认%1设备</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/finger-page.cpp" line="91"/>
        <source>%1 list</source>
        <translation>%1列表</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/finger-page.cpp" line="150"/>
        <source>Start enroll failed,%1</source>
        <translation>开始录入失败,%1</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/finger-page.cpp" line="151"/>
        <location filename="../plugins/authentication/pages/finger-page.cpp" line="204"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/finger-page.cpp" line="198"/>
        <source>The biometric features were successfully recorded. The feature name is:%1</source>
        <translation>特征已成功录入，特征名为:%1</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/finger-page.cpp" line="199"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/finger-page.cpp" line="203"/>
        <source>Failed to record biometrics(%1), Please try again</source>
        <translation>录入特征失败(%1),请重试</translation>
    </message>
    <message>
        <source>%1list</source>
        <translation type="vanished">%1列表</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/finger-page.cpp" line="90"/>
        <source>Default %1 device</source>
        <translation>默认%1设备</translation>
    </message>
</context>
<context>
    <name>FingerprintEnrollDialog</name>
    <message>
        <source>save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Finger Enroll</source>
        <translation type="vanished">指纹录入</translation>
    </message>
    <message>
        <source>This fingerprint is bound to the user(%1)</source>
        <translation type="vanished">该指纹已绑定用户(%1)</translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="vanished">提示</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
</context>
<context>
    <name>FingerprintInputDialog</name>
    <message>
        <source>save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Finger Enroll</source>
        <translation type="vanished">指纹录入</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
</context>
<context>
    <name>FingerprintInputWorker</name>
    <message>
        <source>initializing fingerprint collection environment...</source>
        <translation type="vanished">正在初始化指纹采集环境，请稍等</translation>
    </message>
</context>
<context>
    <name>Fonts</name>
    <message>
        <location filename="../plugins/appearance/pages/font/fonts.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Application Font Settings</source>
        <translation type="vanished">应用程序字体设置</translation>
    </message>
    <message>
        <source>Titlebar Font Settings</source>
        <translation type="vanished">窗口标题字体设置</translation>
    </message>
    <message>
        <source>Monospace Font Settings</source>
        <translation type="vanished">等宽字体设置</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/font/fonts.ui" line="35"/>
        <source>Word size</source>
        <translation>字号</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/font/fonts.ui" line="96"/>
        <source>System font</source>
        <translation>系统字体</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/font/fonts.ui" line="142"/>
        <source>Monospaced font</source>
        <translation>等宽字体</translation>
    </message>
</context>
<context>
    <name>GeneralBioPage</name>
    <message>
        <location filename="../plugins/authentication/utils/general-bio-page.cpp" line="235"/>
        <source>default device</source>
        <translation>默认设备</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/utils/general-bio-page.cpp" line="247"/>
        <source>feature list</source>
        <translation>特征列表</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/utils/general-bio-page.cpp" line="186"/>
        <source>Are you sure you want to delete the feature called %1, Ensure that the Ukey device is inserted; otherwise the information stored in the Ukey will not be deleted</source>
        <translation>您确定要删除特征名为%1的UKey绑定吗?请确保已插入Ukey设备;否则存储在Ukey中的信息将不会被删除</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/utils/general-bio-page.cpp" line="193"/>
        <source>Are you sure you want to delete the feature called %1</source>
        <translation>您确定要删除特征名为%1的特征吗</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/utils/general-bio-page.cpp" line="196"/>
        <source>tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/utils/general-bio-page.cpp" line="220"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/utils/general-bio-page.cpp" line="221"/>
        <source> Failed to enroll feature because the password verification failed！</source>
        <translation>由于密码校验失败，录入特征失败！</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/utils/general-bio-page.cpp" line="156"/>
        <source>Rename Feature</source>
        <translation type="unfinished">重命名特征值</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/utils/general-bio-page.cpp" line="157"/>
        <source>Please enter the renamed feature name</source>
        <translation type="unfinished">请输入特征名</translation>
    </message>
</context>
<context>
    <name>GeneralPage</name>
    <message>
        <location filename="../plugins/keyboard/pages/general-page.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/general-page.ui" line="55"/>
        <source>Capslock Tip</source>
        <translation>大写锁定提示</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/general-page.ui" line="86"/>
        <source>Numlock Tip</source>
        <translation>数字键盘锁定提示</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/general-page.ui" line="123"/>
        <source>Repeat Key</source>
        <translation>重复键</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/general-page.ui" line="130"/>
        <source>(Repeat a key while holding it down)</source>
        <translation>(按住某一键时重复该键)</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/general-page.ui" line="150"/>
        <source>SwitchRepeatKey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/general-page.ui" line="169"/>
        <source>Delay</source>
        <translation>延时</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/general-page.ui" line="191"/>
        <source>SliderRepeatDelay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/general-page.ui" line="215"/>
        <source>Short</source>
        <translation>短</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/general-page.ui" line="235"/>
        <source>Long</source>
        <translation>长</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/general-page.ui" line="253"/>
        <source>Interval</source>
        <translation>速度</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/general-page.ui" line="275"/>
        <source>SliderRepeatInterval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/general-page.ui" line="302"/>
        <source>Slow</source>
        <translation>慢</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/general-page.ui" line="322"/>
        <source>Fast</source>
        <translation>快</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/general-page.ui" line="338"/>
        <location filename="../plugins/keyboard/pages/general-page.cpp" line="54"/>
        <source>Enter repeat characters to test</source>
        <translation>输入重复字符测试</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/general-page.ui" line="357"/>
        <source>EditTestRepeatKey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter characters to test the settings</source>
        <translation type="vanished">输入重复字符测试</translation>
    </message>
</context>
<context>
    <name>GeneralSettingsPage</name>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="14"/>
        <source>GeneralSettingsPage</source>
        <translation>通用设置页面</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="104"/>
        <source>When the power button is pressed</source>
        <translation>按下电源按钮时</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="127"/>
        <source>ComboPowerButtonAction</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="171"/>
        <source>When the suspend button is pressed</source>
        <translation>按下挂起按钮时</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="194"/>
        <source>ComboSuspendAction</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="238"/>
        <source>When closing the lid</source>
        <translation>合上盖子操作</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="279"/>
        <source>ComboCloseLidAction</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="323"/>
        <source>Computer Mode</source>
        <translation>计算机模式</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="394"/>
        <source>Display brightness setting</source>
        <translation>显示亮度设置</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="414"/>
        <source>0%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="444"/>
        <source>SliderDisplayBrightness</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="493"/>
        <source>Color temperature</source>
        <translation>色温</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="524"/>
        <source>Automatic color temperature</source>
        <translation>自动色温</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="595"/>
        <source>cold</source>
        <translation>冷</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="615"/>
        <source>standard</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="635"/>
        <source>warm</source>
        <translation>暖</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="695"/>
        <source>Regard computer as idle after</source>
        <translation>于此时间后视计算机为空闲</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="749"/>
        <source>SliderComputerIdleTime</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="780"/>
        <source>Lock screen when idle</source>
        <translation>计算机空闲时锁定屏幕</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.ui" line="823"/>
        <source>password is required to wake up in standby mode</source>
        <translation>待机时唤醒需要输入密码</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.cpp" line="121"/>
        <location filename="../plugins/power/pages/general-settings-page.cpp" line="134"/>
        <source>shutdown</source>
        <translation>关机</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.cpp" line="122"/>
        <location filename="../plugins/power/pages/general-settings-page.cpp" line="128"/>
        <location filename="../plugins/power/pages/general-settings-page.cpp" line="133"/>
        <source>hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.cpp" line="123"/>
        <location filename="../plugins/power/pages/general-settings-page.cpp" line="127"/>
        <location filename="../plugins/power/pages/general-settings-page.cpp" line="132"/>
        <source>suspend</source>
        <translation>待机</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.cpp" line="124"/>
        <location filename="../plugins/power/pages/general-settings-page.cpp" line="129"/>
        <source>display off</source>
        <translation>关闭显示器</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.cpp" line="125"/>
        <location filename="../plugins/power/pages/general-settings-page.cpp" line="130"/>
        <location filename="../plugins/power/pages/general-settings-page.cpp" line="135"/>
        <source>do nothing</source>
        <translation>不执行操作</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.cpp" line="414"/>
        <source>ERROR</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.cpp" line="530"/>
        <source>%1hour</source>
        <translation>%1小时</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/general-settings-page.cpp" line="534"/>
        <source>%1minute</source>
        <translation>%1分钟</translation>
    </message>
</context>
<context>
    <name>GeneralSettingsSubItem</name>
    <message>
        <source>General Settings</source>
        <translation type="vanished">通用设置</translation>
    </message>
</context>
<context>
    <name>GeneralSubItem</name>
    <message>
        <location filename="../plugins/keyboard/general-subitem.h" line="46"/>
        <source>Keyboard General Option</source>
        <translation>键盘通用选项</translation>
    </message>
</context>
<context>
    <name>GroupInfoPage</name>
    <message>
        <location filename="../plugins/group/src/pages/group-info-page/group-info-page.ui" line="32"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/group-info-page/group-info-page.ui" line="136"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/group-info-page/group-info-page.ui" line="236"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/group-info-page/group-info-page.ui" line="335"/>
        <source>Member List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/group-info-page/group-info-page.ui" line="435"/>
        <source>Add User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/group-info-page/group-info-page.ui" line="482"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/group-info-page/group-info-page.ui" line="564"/>
        <source>Add Member</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/group-info-page/group-info-page.ui" line="679"/>
        <source>Save</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/group-info-page/group-info-page.ui" line="726"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/group-info-page/group-info-page.cpp" line="123"/>
        <source>Please input keys for search...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/pages/group-info-page/group-info-page.cpp" line="247"/>
        <location filename="../plugins/group/src/pages/group-info-page/group-info-page.cpp" line="259"/>
        <location filename="../plugins/group/src/pages/group-info-page/group-info-page.cpp" line="269"/>
        <location filename="../plugins/group/src/pages/group-info-page/group-info-page.cpp" line="280"/>
        <source>Error</source>
        <translation type="unfinished">错误</translation>
    </message>
</context>
<context>
    <name>GroupSubItem</name>
    <message>
        <location filename="../plugins/group/src/group-subitem.cpp" line="44"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/group-subitem.cpp" line="76"/>
        <source>Creat group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/group-subitem.cpp" line="77"/>
        <source>Change group name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/group-subitem.cpp" line="78"/>
        <source>Add group member</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HardWorker</name>
    <message>
        <location filename="../plugins/account/utils/hard-worker.cpp" line="46"/>
        <source>Create User failed</source>
        <translation>创建用户失败</translation>
    </message>
    <message>
        <location filename="../plugins/account/utils/hard-worker.cpp" line="150"/>
        <source> update password failed</source>
        <translation>更新密码失败</translation>
    </message>
    <message>
        <location filename="../plugins/account/utils/hard-worker.cpp" line="176"/>
        <source>icon file</source>
        <translation>头像</translation>
    </message>
    <message>
        <location filename="../plugins/account/utils/hard-worker.cpp" line="187"/>
        <source>userName type</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../plugins/account/utils/hard-worker.cpp" line="198"/>
        <source>locked</source>
        <translation>启用状态</translation>
    </message>
    <message>
        <location filename="../plugins/account/utils/hard-worker.cpp" line="208"/>
        <source>Failed to update user properties,%1</source>
        <translation>更新用户属性失败，%1</translation>
    </message>
    <message>
        <location filename="../plugins/account/utils/hard-worker.cpp" line="230"/>
        <source>Failed to delete user,%1</source>
        <translation>删除用户失败，%1</translation>
    </message>
    <message>
        <source>password</source>
        <translation type="vanished">密码</translation>
    </message>
    <message>
        <source>home directory</source>
        <translation type="vanished">用户目录</translation>
    </message>
    <message>
        <source>shell</source>
        <translation type="vanished">shell</translation>
    </message>
    <message>
        <source>icon</source>
        <translation type="vanished">头像</translation>
    </message>
    <message>
        <source>Failed to set user attributes</source>
        <translation type="vanished">设置用户属性失败</translation>
    </message>
    <message>
        <source>account type</source>
        <translation type="vanished">帐户类型</translation>
    </message>
    <message>
        <source>Failed to update user properties(%1)</source>
        <translation type="vanished">更新用户属性失败(%1)</translation>
    </message>
    <message>
        <source>Failed to delete user</source>
        <translation type="vanished">删除用户失败</translation>
    </message>
    <message>
        <location filename="../plugins/account/utils/hard-worker.cpp" line="59"/>
        <source>Failed to connect to the account management service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/hard-worker.cpp" line="38"/>
        <source>Create Group failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/hard-worker.cpp" line="92"/>
        <source>Failed to delete group,%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/hard-worker.cpp" line="113"/>
        <location filename="../plugins/group/src/hard-worker.cpp" line="135"/>
        <source> add user to group failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/hard-worker.cpp" line="158"/>
        <source> change group name failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/group/src/hard-worker.cpp" line="169"/>
        <source> change group name failed, the new group name is occupied</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HardwareInformation</name>
    <message>
        <location filename="../plugins/system/pages/hardware-information/hardware-information.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/hardware-information/hardware-information.ui" line="147"/>
        <source>CPU:</source>
        <translation>CPU:</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/hardware-information/hardware-information.ui" line="167"/>
        <source>LabelCpuInfo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/hardware-information/hardware-information.ui" line="170"/>
        <location filename="../plugins/system/pages/hardware-information/hardware-information.ui" line="233"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/hardware-information/hardware-information.ui" line="210"/>
        <source>Memory:</source>
        <translation>内存:</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/hardware-information/hardware-information.ui" line="230"/>
        <source>LabelMemoryInfo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/hardware-information/hardware-information.ui" line="273"/>
        <source>Hard disk:</source>
        <translation>硬盘:</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/hardware-information/hardware-information.ui" line="351"/>
        <source>Graphics card:</source>
        <translation>显卡:</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/hardware-information/hardware-information.ui" line="426"/>
        <source>Network card:</source>
        <translation>网卡:</translation>
    </message>
    <message>
        <source>Copyright ©</source>
        <translation type="vanished">版权所有 ©</translation>
    </message>
    <message>
        <source>KylinSec. All rights reserved.</source>
        <translation type="vanished">KylinSec.保留所有权利.</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/hardware-information/hardware-information.cpp" line="109"/>
        <location filename="../plugins/system/pages/hardware-information/hardware-information.cpp" line="110"/>
        <location filename="../plugins/system/pages/hardware-information/hardware-information.cpp" line="118"/>
        <source>Unknow</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/hardware-information/hardware-information.cpp" line="177"/>
        <source>%1 GB (%2 GB available)</source>
        <translation>%1 GB (%2 GB 可用)</translation>
    </message>
</context>
<context>
    <name>HardwareInformationWidget</name>
    <message>
        <source>CPU:</source>
        <translation type="vanished">CPU:</translation>
    </message>
    <message>
        <source>Memory:</source>
        <translation type="vanished">内存:</translation>
    </message>
    <message>
        <source>Hard disk:</source>
        <translation type="vanished">硬盘:</translation>
    </message>
    <message>
        <source>Graphics card:</source>
        <translation type="vanished">显卡:</translation>
    </message>
    <message>
        <source>Network card:</source>
        <translation type="vanished">网卡:</translation>
    </message>
    <message>
        <source>Unknow</source>
        <translation type="vanished">未知</translation>
    </message>
</context>
<context>
    <name>HardwareSubItem</name>
    <message>
        <location filename="../plugins/system/hardware-subitem.h" line="33"/>
        <source>Hardware Information</source>
        <translation>硬件信息</translation>
    </message>
</context>
<context>
    <name>IconThemePage</name>
    <message>
        <location filename="../plugins/appearance/pages/theme/icon/icon-theme-page.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/theme/icon/icon-theme-page.ui" line="35"/>
        <source>Icon Themes Setting</source>
        <translation type="unfinished">图标主题设置</translation>
    </message>
</context>
<context>
    <name>IconThemes</name>
    <message>
        <source>Icon Themes Setting</source>
        <translation type="vanished">图标主题设置</translation>
    </message>
    <message>
        <source>Faild</source>
        <translation type="vanished">失败</translation>
    </message>
    <message>
        <source>Set icon themes failed!</source>
        <translation type="vanished">设置图标主题失败!</translation>
    </message>
</context>
<context>
    <name>IdentificationRenameDialog</name>
    <message>
        <source>Rename Feature</source>
        <translation type="vanished">重命名特征值</translation>
    </message>
    <message>
        <source>Please enter the renamed feature name</source>
        <translation type="vanished">请输入特征名</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>ImageSelector</name>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/widget/image-selector.cpp" line="104"/>
        <source>Add Image Failed</source>
        <translation>添加壁纸失败</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/widget/image-selector.cpp" line="105"/>
        <source>The image already exists!</source>
        <translation>该壁纸已存在!</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/widget/image-selector.cpp" line="193"/>
        <source>Delete image</source>
        <translation>删除壁纸</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/widget/image-selector.cpp" line="194"/>
        <source>Are you sure you want to delete this picture?</source>
        <translation>您确定要删除此壁纸?</translation>
    </message>
</context>
<context>
    <name>InputDialog</name>
    <message>
        <location filename="../lib/common-widgets/input-dialog/input-dialog.cpp" line="102"/>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../lib/common-widgets/input-dialog/input-dialog.cpp" line="111"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>InputPage</name>
    <message>
        <location filename="../plugins/audio/src/plugin/input-page.ui" line="14"/>
        <source>InputPage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/input-page.ui" line="40"/>
        <source>Input cards</source>
        <translation>输入声卡</translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/input-page.ui" line="64"/>
        <source>Input devices</source>
        <translation>输入设备</translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/input-page.ui" line="90"/>
        <source>ComboBoxInputDevices</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/input-page.ui" line="107"/>
        <source>Input volume</source>
        <translation>输入音量</translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/input-page.ui" line="158"/>
        <source>SliderVolumeSetting</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/input-page.ui" line="178"/>
        <source>Feedback volume</source>
        <translation>反馈音量</translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/input-page.cpp" line="294"/>
        <source>No input device detected</source>
        <translation>未检测到输入设备</translation>
    </message>
</context>
<context>
    <name>Ipv4Widget</name>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="14"/>
        <source>Ipv4Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="37"/>
        <source>IPV4 Method</source>
        <translation>IPV4方法</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="56"/>
        <source>ComboBoxIpv4Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="88"/>
        <source>IP Address</source>
        <translation>IP地址</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="107"/>
        <source>EditIpv4Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="121"/>
        <source>Net Mask</source>
        <translation>子网掩码</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="140"/>
        <source>EditIpv4Netmask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="154"/>
        <source>Gateway</source>
        <translation>网关</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="173"/>
        <source>EditIpv4Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="190"/>
        <source>DNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.ui" line="209"/>
        <source>EditIpv4PreferredDNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="38"/>
        <source>Auto</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="39"/>
        <source>Manual</source>
        <translation>手动</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="41"/>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="42"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="43"/>
        <source>Please separate multiple DNS entries by semicolon</source>
        <translation>请用分号分隔多个DNS</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="243"/>
        <source>Ipv4 DNS invalid</source>
        <translation>无效的Ipv4 DNS</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="262"/>
        <source>Ipv4 address can not be empty</source>
        <translation>Ipv4地址不能为空</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="272"/>
        <source>Ipv4 Address invalid</source>
        <translation>无效的Ipv4地址</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="282"/>
        <source>NetMask can not be empty</source>
        <translation>子网掩码不能为空</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="292"/>
        <source>Netmask invalid</source>
        <translation>无效的子网掩码</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv4-widget.cpp" line="304"/>
        <source>Ipv4 Gateway invalid</source>
        <translation>无效的Ipv4网关</translation>
    </message>
    <message>
        <source>Preferred DNS</source>
        <translation type="vanished">首选DNS</translation>
    </message>
    <message>
        <source>Alternate DNS</source>
        <translation type="vanished">备选DNS</translation>
    </message>
    <message>
        <source>Ipv4 Preferred DNS invalid</source>
        <translation type="vanished">无效的Ipv4首选DNS</translation>
    </message>
    <message>
        <source>Ipv4 Alternate DNS invalid</source>
        <translation type="vanished">无效的Ipv4备选DNS</translation>
    </message>
</context>
<context>
    <name>Ipv6Widget</name>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="14"/>
        <source>Ipv6Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="40"/>
        <source>IPV6 Method</source>
        <translation>IPV6方法</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="59"/>
        <source>ComboBoxIpv6Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="91"/>
        <source>IP Address</source>
        <translation>IP地址</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="110"/>
        <source>EditIpv6Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="124"/>
        <source>Prefix</source>
        <translation>前缀</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="143"/>
        <source>SpinBoxIpv6Prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="157"/>
        <source>Gateway</source>
        <translation>网关</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="176"/>
        <source>EditIpv6Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="193"/>
        <source> DNS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.ui" line="212"/>
        <source>EditIpv6PreferredDNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="37"/>
        <source>Auto</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="38"/>
        <source>Manual</source>
        <translation>手动</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="39"/>
        <source>Ignored</source>
        <translation>忽略</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="41"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="44"/>
        <source>Please separate multiple DNS entries by semicolon</source>
        <translation>请用分号分隔多个DNS</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="234"/>
        <source>Ipv6 DNS invalid</source>
        <translation>无效的Ipv6 DNS</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="250"/>
        <source>Ipv6 address can not be empty</source>
        <translation>Ipv6地址不能为空</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="261"/>
        <source>Ipv6 address invalid</source>
        <translation>无效的Ipv6地址</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/ipv6-widget.cpp" line="274"/>
        <source>Ipv6 Gateway invalid</source>
        <translation>无效的Ipv6网关</translation>
    </message>
    <message>
        <source>Preferred DNS</source>
        <translation type="vanished">首选DNS</translation>
    </message>
    <message>
        <source>Alternate DNS</source>
        <translation type="vanished">备选DNS</translation>
    </message>
    <message>
        <source>Ipv6 Preferred DNS invalid</source>
        <translation type="vanished">无效的Ipv6首选DNS</translation>
    </message>
    <message>
        <source>Ipv6 Alternate DNS invalid</source>
        <translation type="vanished">无效的Ipv6备选DNS</translation>
    </message>
</context>
<context>
    <name>IrisPage</name>
    <message>
        <location filename="../plugins/authentication/pages/iris-page.cpp" line="66"/>
        <source>Default Iris device</source>
        <translation>默认虹膜设备</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/iris-page.cpp" line="67"/>
        <source>Iris feature list</source>
        <translation>虹膜特征列表</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/iris-page.cpp" line="65"/>
        <source>iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/iris-page.cpp" line="99"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/iris-page.cpp" line="147"/>
        <source>Start enroll failed,%1</source>
        <translation>开始录入失败,%1</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/iris-page.cpp" line="148"/>
        <location filename="../plugins/authentication/pages/iris-page.cpp" line="179"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/iris-page.cpp" line="173"/>
        <source>The biometric features were successfully recorded. The feature name is:%1</source>
        <translation>特征已成功录入，特征名为:%1</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/iris-page.cpp" line="174"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/iris-page.cpp" line="178"/>
        <source>Failed to record biometrics(%1), Please try again</source>
        <translation>录入特征失败(%1),请重试</translation>
    </message>
</context>
<context>
    <name>KcpInterface</name>
    <message>
        <source>Warning</source>
        <translation type="obsolete">警告</translation>
    </message>
    <message>
        <source>load qss file failed</source>
        <translation type="vanished">加载qss文件失败</translation>
    </message>
</context>
<context>
    <name>KeybindingSubItem</name>
    <message>
        <location filename="../plugins/keybinding/keybinding-subitem.h" line="46"/>
        <source>Keybinding</source>
        <translation>快捷键</translation>
    </message>
</context>
<context>
    <name>KeycodeTranslator</name>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="140"/>
        <source>None</source>
        <translation>暂无</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="144"/>
        <source>disabled</source>
        <translation>禁用</translation>
    </message>
</context>
<context>
    <name>KiranAccountManager</name>
    <message>
        <source>disable</source>
        <translation type="vanished">禁用</translation>
    </message>
    <message>
        <source>enable</source>
        <translation type="vanished">启用</translation>
    </message>
    <message>
        <source>Create new user</source>
        <translation type="vanished">创建新用户</translation>
    </message>
    <message>
        <source>User Manager</source>
        <translation type="vanished">帐户管理工具</translation>
    </message>
    <message>
        <source>Create new account</source>
        <translation type="vanished">创建新用户</translation>
    </message>
</context>
<context>
    <name>KiranAvatarEditor</name>
    <message>
        <location filename="../libexec/avatar-editor/src/kiran-avatar-editor.cpp" line="42"/>
        <source>Avatar Editor</source>
        <translation>头像编辑器</translation>
    </message>
    <message>
        <location filename="../libexec/avatar-editor/src/kiran-avatar-editor.cpp" line="91"/>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../libexec/avatar-editor/src/kiran-avatar-editor.cpp" line="112"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>KiranCPanelMouse</name>
    <message>
        <source>Mouse and TouchPad</source>
        <translation type="vanished">鼠标和触摸板</translation>
    </message>
</context>
<context>
    <name>KiranCPanelMouseWidget</name>
    <message>
        <source>Select Mouse Hand</source>
        <translation type="vanished">选择鼠标手持模式</translation>
    </message>
    <message>
        <source>Mouse Motion Acceleration</source>
        <translation type="vanished">鼠标移动加速</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">是否为自然滚动</translation>
    </message>
    <message>
        <source>Middle Emulation Enabled</source>
        <translation type="vanished">同时按下左右键模拟中键</translation>
    </message>
    <message>
        <source>Touchpad Enabled</source>
        <translation type="vanished">禁用触摸板</translation>
    </message>
    <message>
        <source>Select TouchPad Hand</source>
        <translation type="vanished">选择触摸板使用模式</translation>
    </message>
    <message>
        <source>TouchPad Motion Acceleration</source>
        <translation type="vanished">触摸板移动加速</translation>
    </message>
    <message>
        <source>Select Click Method</source>
        <translation type="vanished">设置点击触摸板方式</translation>
    </message>
    <message>
        <source>Select Scroll Method</source>
        <translation type="vanished">滚动窗口方式</translation>
    </message>
    <message>
        <source>Enabled while Typing</source>
        <translation type="vanished">打字时触摸板禁用</translation>
    </message>
    <message>
        <source>Tap to Click</source>
        <translation type="vanished">轻击(不按下)触摸板功能是否生效</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">重置</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">保存</translation>
    </message>
    <message>
        <source>Mouse Settings</source>
        <translation type="vanished">鼠标设置</translation>
    </message>
    <message>
        <source>TouchPad Settings</source>
        <translation type="vanished">触摸板设置</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">标准</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">右手模式</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">左手模式</translation>
    </message>
    <message>
        <source>Press and Tap</source>
        <translation type="vanished">按键和轻触</translation>
    </message>
    <message>
        <source>Tap</source>
        <translation type="vanished">轻触</translation>
    </message>
    <message>
        <source>Two Finger Scroll</source>
        <translation type="vanished">两指滑动</translation>
    </message>
    <message>
        <source>Edge Scroll</source>
        <translation type="vanished">边缘滑动</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">低速</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">快速</translation>
    </message>
</context>
<context>
    <name>KiranCollapse</name>
    <message>
        <location filename="../lib/common-widgets/kiran-collapse/kiran-collapse.ui" line="20"/>
        <source>ListExpansionSpace</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KiranCpanelAppearance</name>
    <message>
        <source>Wallpaper Setting</source>
        <translation type="vanished">壁纸设置</translation>
    </message>
    <message>
        <source>Theme Setting</source>
        <translation type="vanished">主题设置</translation>
    </message>
    <message>
        <source>Font Setting</source>
        <translation type="vanished">字体设置</translation>
    </message>
</context>
<context>
    <name>KiranDatePickerWidget</name>
    <message>
        <location filename="../plugins/timedate/widgets/kiran-date-picker-widget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KiranGroupManager</name>
    <message>
        <location filename="../plugins/group/src/kiran-group-manager.cpp" line="141"/>
        <source>Create new group</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KiranModuleWidget</name>
    <message>
        <source>Warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>The edited content in %1 is not saved. After switching, the edited content will be lost. Are you sure you want to save？</source>
        <translation type="vanished">%1中编辑的内容未保存，切换后编辑的内容将会丢失。您确定要保存吗?</translation>
    </message>
    <message>
        <location filename="../lib/common-widgets/kiran-module-widget/kiran-module-widget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KiranSystemWidget</name>
    <message>
        <source>kiran-system-imformation</source>
        <translation type="vanished">系统信息</translation>
    </message>
    <message>
        <source>保存</source>
        <translation type="obsolete">保存</translation>
    </message>
</context>
<context>
    <name>KiranTimeDateWidget</name>
    <message>
        <location filename="../plugins/timedate/kiran-timedate-widget.ui" line="14"/>
        <source>KiranTimeDateWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/timedate/kiran-timedate-widget.ui" line="171"/>
        <source>Automatic synchronizetion</source>
        <translation>自动同步</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/kiran-timedate-widget.cpp" line="120"/>
        <source>Change Time Zone</source>
        <translation>更改时区</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/kiran-timedate-widget.cpp" line="131"/>
        <source>Set Time Manually</source>
        <translation>手动设置时间</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/kiran-timedate-widget.cpp" line="147"/>
        <source>Time date format setting</source>
        <translation>日期时间格式设置</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/kiran-timedate-widget.cpp" line="196"/>
        <source>%1(%2)</source>
        <translation>%1时间(%2)</translation>
    </message>
</context>
<context>
    <name>KiranTimePickerWidget</name>
    <message>
        <location filename="../plugins/timedate/widgets/kiran-time-picker-widget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KiranTimeZone</name>
    <message>
        <location filename="../plugins/timedate/widgets/kiran-time-zone.ui" line="23"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/timedate/widgets/kiran-time-zone.cpp" line="95"/>
        <source>Search in all time zones...</source>
        <translation>在所有时区中搜索...</translation>
    </message>
</context>
<context>
    <name>KiranTimeZoneItem</name>
    <message>
        <location filename="../plugins/timedate/widgets/kiran-time-zone-item.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/timedate/widgets/kiran-time-zone-item.cpp" line="70"/>
        <source>No search results, please search again...</source>
        <translation>无搜索结果，请重新搜索...</translation>
    </message>
</context>
<context>
    <name>KiranTimeZoneList</name>
    <message>
        <location filename="../plugins/timedate/widgets/kiran-time-zone-list.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KiranTips</name>
    <message>
        <location filename="../plugins/group/src/widgets/kiran-tips.ui" line="29"/>
        <location filename="../lib/common-widgets/kiran-tips/kiran-tips.ui" line="29"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KylinsecLogo</name>
    <message>
        <location filename="../plugins/system/kylinsec-logo.cpp" line="36"/>
        <source>Copyright ©</source>
        <translation type="unfinished">版权所有 ©</translation>
    </message>
    <message>
        <location filename="../plugins/system/kylinsec-logo.cpp" line="36"/>
        <source>KylinSec. All rights reserved.</source>
        <translation type="unfinished">KylinSec.保留所有权利.</translation>
    </message>
</context>
<context>
    <name>LayoutItem</name>
    <message>
        <location filename="../plugins/keyboard/utils/layout-item.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LayoutList</name>
    <message>
        <location filename="../plugins/keyboard/utils/layout-list.ui" line="20"/>
        <source>LayoutList</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LayoutPage</name>
    <message>
        <location filename="../plugins/keyboard/pages/layout-page.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/layout-page.ui" line="67"/>
        <source>Select Kayboard Layout</source>
        <translation>选择布局</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/layout-page.ui" line="105"/>
        <location filename="../plugins/keyboard/pages/layout-page.cpp" line="383"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/layout-page.ui" line="206"/>
        <location filename="../plugins/keyboard/pages/layout-page.cpp" line="265"/>
        <source>Add Layout</source>
        <translation>添加布局</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/layout-page.ui" line="297"/>
        <source>ButtonAddLayout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/layout-page.ui" line="300"/>
        <source>Addition</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/layout-page.ui" line="341"/>
        <source>ButtonReturn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/layout-page.ui" line="344"/>
        <source>Return</source>
        <translation type="unfinished">返回</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/layout-page.cpp" line="112"/>
        <location filename="../plugins/keyboard/pages/layout-page.cpp" line="287"/>
        <location filename="../plugins/keyboard/pages/layout-page.cpp" line="326"/>
        <location filename="../plugins/keyboard/pages/layout-page.cpp" line="359"/>
        <source>Failed</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/layout-page.cpp" line="112"/>
        <source>You have added this keyboard layout!</source>
        <translation>您已经添加过该布局</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/layout-page.cpp" line="288"/>
        <source>The %1 keyboard layout does not exist!</source>
        <translation>该 %1 键盘布局不存在!</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/layout-page.cpp" line="327"/>
        <source>The keyboard layout is currently in use and cannot be deleted!</source>
        <translation>该布局目前正在使用，无法删除!</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/layout-page.cpp" line="339"/>
        <source>Delete Layout</source>
        <translation>删除布局</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/layout-page.cpp" line="360"/>
        <source>You do not appear to have added %1 keyboard layout!</source>
        <translation>您似乎没有添加 %1 键盘布局！</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/pages/layout-page.cpp" line="374"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
</context>
<context>
    <name>LayoutSubItem</name>
    <message>
        <location filename="../plugins/keyboard/layout-subitem.h" line="46"/>
        <source>Keyboard Layout</source>
        <translation>键盘布局</translation>
    </message>
</context>
<context>
    <name>LicenseAgreement</name>
    <message>
        <location filename="../plugins/system/pages/system-information/license-agreement.ui" line="32"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/license-agreement.ui" line="88"/>
        <source>BrowserLicense</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/license-agreement.ui" line="103"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/license-agreement.ui" line="150"/>
        <source>ButtonExportLicense</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/license-agreement.ui" line="153"/>
        <source>Export</source>
        <translation>导出</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/license-agreement.ui" line="194"/>
        <source>ButtonCloseLicense</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/license-agreement.ui" line="197"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/license-agreement.cpp" line="88"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/license-agreement.cpp" line="90"/>
        <source>PDF(*.pdf)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/license-agreement.cpp" line="99"/>
        <source>Export License</source>
        <translation>导出协议</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/license-agreement.cpp" line="100"/>
        <source>Export License failed!</source>
        <translation>导出协议失败!</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/license-agreement.cpp" line="142"/>
        <source>User End License Agreement</source>
        <translation>最终用户许可协议</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/license-agreement.cpp" line="161"/>
        <location filename="../plugins/system/pages/system-information/license-agreement.cpp" line="174"/>
        <location filename="../plugins/system/pages/system-information/license-agreement.cpp" line="235"/>
        <location filename="../plugins/system/pages/system-information/license-agreement.cpp" line="243"/>
        <location filename="../plugins/system/pages/system-information/license-agreement.cpp" line="279"/>
        <location filename="../plugins/system/pages/system-information/license-agreement.cpp" line="286"/>
        <source>None</source>
        <translation>暂无</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/license-agreement.cpp" line="190"/>
        <source>Version License</source>
        <translation>版本协议</translation>
    </message>
    <message>
        <source>Export EULA</source>
        <translation type="obsolete">导出最终用户许可协议</translation>
    </message>
    <message>
        <source>Export EULA failed!</source>
        <translation type="obsolete">导出最终用户许可协议失败!</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/license-agreement.cpp" line="269"/>
        <location filename="../plugins/system/pages/system-information/license-agreement.cpp" line="271"/>
        <source>Privacy Policy</source>
        <translation>隐私协议</translation>
    </message>
</context>
<context>
    <name>LicenseInfoWidget</name>
    <message>
        <source>Machine Code:</source>
        <translation type="vanished">机器码:</translation>
    </message>
    <message>
        <source>Activation Code:</source>
        <translation type="vanished">激活码:</translation>
    </message>
    <message>
        <source>Activation Information</source>
        <translation type="vanished">激活信息</translation>
    </message>
    <message>
        <source>Can&apos;t get machine code</source>
        <translation type="vanished">无法获取到机器码</translation>
    </message>
    <message>
        <source>Can&apos;t get activation code</source>
        <translation type="vanished">无法获取到激活码</translation>
    </message>
</context>
<context>
    <name>LicenseInformation</name>
    <message>
        <source>Installation time:</source>
        <translation type="vanished">安装时间:</translation>
    </message>
    <message>
        <source>Activation status:</source>
        <translation type="vanished">激活状态:</translation>
    </message>
    <message>
        <source>Expiry date:</source>
        <translation type="vanished">质保期:</translation>
    </message>
    <message>
        <source>Contact Us:</source>
        <translation type="vanished">联系我们:</translation>
    </message>
    <message>
        <source>Unknow</source>
        <translation type="vanished">未知</translation>
    </message>
    <message>
        <source>Can&apos;t get activation information</source>
        <translation type="vanished">无法获取激活信息</translation>
    </message>
    <message>
        <source>Activate</source>
        <translation type="vanished">激活</translation>
    </message>
    <message>
        <source>The current time is illegal</source>
        <translation type="vanished">当前时间不合法</translation>
    </message>
    <message>
        <source>Less than the installation time</source>
        <translation type="vanished">小于安装时间</translation>
    </message>
    <message>
        <source>Not activated. Trail expiration: </source>
        <translation type="vanished">未激活.试用到期: </translation>
    </message>
    <message>
        <source>get service status failed</source>
        <translation type="vanished">获取服务状态信息失败</translation>
    </message>
    <message>
        <source>Not yet</source>
        <translation type="vanished">暂无</translation>
    </message>
    <message>
        <source>Activated</source>
        <translation type="vanished">已激活</translation>
    </message>
    <message>
        <source>Forever</source>
        <translation type="vanished">永久授权</translation>
    </message>
    <message>
        <source>Copyright ©</source>
        <translation type="vanished">版权所有 ©</translation>
    </message>
    <message>
        <source>KylinSec. All rights reserved.</source>
        <translation type="vanished">KylinSec.保留所有权利.</translation>
    </message>
</context>
<context>
    <name>ListExpansionSpace</name>
    <message>
        <location filename="../lib/common-widgets/kiran-collapse/list-expansion-space.ui" line="20"/>
        <source>ListExpansionSpace</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Media Key</name>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="69"/>
        <source>Audio Play</source>
        <translation>音频播放</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="70"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="71"/>
        <source>WWW</source>
        <translation>万维网</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="72"/>
        <source>Audio Lower Volume</source>
        <translation>减小音量</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="73"/>
        <source>Audio Raise Volume</source>
        <translation>增大音量</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="74"/>
        <source>Mic Mute</source>
        <translation>输入静音</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="75"/>
        <source>Audio Stop</source>
        <translation>音频停止</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="76"/>
        <source>Explorer</source>
        <translation>浏览</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="77"/>
        <source>Calculator</source>
        <translation>计算器</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="78"/>
        <source>Audio Mute</source>
        <translation>音频暂停</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="79"/>
        <source>Audio Pause</source>
        <translation>音频暂停</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="80"/>
        <source>Audio Prev</source>
        <translation>音频上一个</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="81"/>
        <source>Audio Media</source>
        <translation>音频媒体</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="82"/>
        <source>Audio Next</source>
        <translation>音频下一个</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="83"/>
        <source>Mail</source>
        <translation>邮件</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="84"/>
        <source>Tools</source>
        <translation>工具</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/keycode-translator.cpp" line="85"/>
        <source>Eject</source>
        <translation>弹出</translation>
    </message>
</context>
<context>
    <name>MonthSpinBox</name>
    <message>
        <location filename="../plugins/timedate/widgets/date-spinbox.h" line="51"/>
        <source>MMMM</source>
        <translation type="unfinished">MMMM</translation>
    </message>
</context>
<context>
    <name>MousePage</name>
    <message>
        <location filename="../plugins/mouse/pages/mouse-page.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/mouse-page.ui" line="68"/>
        <source>Select Mouse Hand</source>
        <translation>选择鼠标手持模式</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/mouse-page.ui" line="103"/>
        <source>ComboSelectMouseHand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/mouse-page.ui" line="126"/>
        <source>Mouse Motion Acceleration</source>
        <translation>鼠标移动加速</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/mouse-page.ui" line="133"/>
        <source>SliderMouseMotionAcceleration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/mouse-page.ui" line="163"/>
        <source>Slow</source>
        <translation>慢</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/mouse-page.ui" line="183"/>
        <source>Fast</source>
        <translation>快</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/mouse-page.ui" line="216"/>
        <source>Natural Scroll</source>
        <translation>是否为自然滚动</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/mouse-page.ui" line="236"/>
        <source>SwitchMouseNatturalScroll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/mouse-page.ui" line="269"/>
        <source>Middle Emulation Enabled</source>
        <translation>同时按下左右键模拟中键</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/mouse-page.ui" line="289"/>
        <source>SwitchMiddleEmulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/mouse-page.ui" line="335"/>
        <source>Test mouse wheel direction</source>
        <translation>鼠标滚轮方向测试</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/mouse-page.ui" line="372"/>
        <source>This is line 1 of the test text
This is line 2 of the test text
This is line 3 of the test text
This is line 4 of the test text
This is line 5 of the test text
This is line 6 of the test text
This is line 7 of the test text
This is line 8 of the test text
This is line 9 of the test text
This is line 10 of the test text
This is line 11 of the test text
This is line 12 of the test text
This is line 13 of the test text
This is line 14 of the test text
This is line 15 of the test text
This is line 16 of the test text
This is line 17 of the test text
This is line 18 of the test text
This is line 19 of the test text
This is line 20 of the test text
This is line 21 of the test text
This is line 22 of the test text
This is line 23 of the test text
This is line 24 of the test text
This is line 25 of the test text
This is line 26 of the test text
This is line 27 of the test text
This is line 28 of the test text
This is line 29 of the test text
This is line 30 of the test text
This is line 31 of the test text
This is line 32 of the test text
This is line 33 of the test text
This is line 34 of the test text
This is line 35 of the test text
This is line 36 of the test text
This is line 37 of the test text
This is line 38 of the test text
This is line 39 of the test text
This is line 40 of the test text
This is line 41 of the test text
This is line 42 of the test text
This is line 43 of the test text
This is line 44 of the test text
This is line 45 of the test text
This is line 46 of the test text
This is line 47 of the test text
This is line 48 of the test text
This is line 49 of the test text
This is line 50 of the test text</source>
        <translation>这是第1行测试文字
这是第2行测试文字
这是第3行测试文字
这是第4行测试文字
这是第5行测试文字
这是第6行测试文字
这是第7行测试文字
这是第8行测试文字
这是第9行测试文字
这是第10行测试文字
这是第11行测试文字
这是第12行测试文字
这是第13行测试文字
这是第14行测试文字
这是第15行测试文字
这是第16行测试文字
这是第17行测试文字
这是第18行测试文字
这是第19行测试文字
这是第20行测试文字
这是第21行测试文字
这是第22行测试文字
这是第23行测试文字
这是第24行测试文字
这是第25行测试文字
这是第26行测试文字
这是第27行测试文字
这是第28行测试文字
这是第29行测试文字
这是第30行测试文字
这是第31行测试文字
这是第32行测试文字
这是第33行测试文字
这是第34行测试文字
这是第35行测试文字
这是第36行测试文字
这是第37行测试文字
这是第38行测试文字
这是第39行测试文字
这是第40行测试文字
这是第41行测试文字
这是第42行测试文字
这是第43行测试文字
这是第44行测试文字
这是第45行测试文字
这是第46行测试文字
这是第47行测试文字
这是第48行测试文字
这是第49行测试文字
这是第50行测试文字</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/mouse-page.cpp" line="80"/>
        <source>Right Hand Mode</source>
        <translation>右手模式</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/mouse-page.cpp" line="80"/>
        <source>Left Hand Mode</source>
        <translation>左手模式</translation>
    </message>
</context>
<context>
    <name>MouseSettings</name>
    <message>
        <source>Select Mouse Hand</source>
        <translation type="vanished">选择鼠标手持模式</translation>
    </message>
    <message>
        <source>Mouse Motion Acceleration</source>
        <translation type="vanished">鼠标移动加速</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">是否为自然滚动</translation>
    </message>
    <message>
        <source>Middle Emulation Enabled</source>
        <translation type="vanished">同时按下左右键模拟中键</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">右手模式</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">左手模式</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">慢</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">标准</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">快</translation>
    </message>
</context>
<context>
    <name>MouseSubItem</name>
    <message>
        <location filename="../plugins/mouse/mouse-subitem.h" line="46"/>
        <source>Mouse Settings</source>
        <translation>鼠标设置</translation>
    </message>
</context>
<context>
    <name>NetworkSubItem</name>
    <message>
        <location filename="../plugins/network/src/plugin/network-subitem.cpp" line="126"/>
        <source>Wired Network %1</source>
        <translation>有线网络 %1</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/network-subitem.cpp" line="130"/>
        <source>Wired Network</source>
        <translation>有线网络</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/network-subitem.cpp" line="139"/>
        <source>Wireless Network %1</source>
        <translation>无线网络  %1</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/network-subitem.cpp" line="144"/>
        <source>Wireless Network</source>
        <translation>无线网络</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/network-subitem.cpp" line="151"/>
        <source>VPN</source>
        <translation>VPN</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/network-subitem.cpp" line="152"/>
        <source>Network Details</source>
        <translation>网络详情</translation>
    </message>
</context>
<context>
    <name>NetworkTray</name>
    <message>
        <location filename="../plugins/network/src/tray/network-tray.cpp" line="175"/>
        <source>Network settings</source>
        <translation>网络设置</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/network-tray.cpp" line="239"/>
        <location filename="../plugins/network/src/tray/network-tray.cpp" line="518"/>
        <source>Network unavailable</source>
        <translation>网络不可用</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/network-tray.h" line="119"/>
        <location filename="../plugins/network/src/tray/network-tray.h" line="127"/>
        <source>The network is connected, but you cannot access the Internet</source>
        <translation>网络已连接，但不能访问互联网</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/network-tray.h" line="131"/>
        <source>Network not connected</source>
        <translation>网络已断开</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/network-tray.cpp" line="482"/>
        <source>Wired network card: %1 available</source>
        <translation>有线网卡: %1 可用</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/network-tray.cpp" line="487"/>
        <source>Wireless network card: %1 available</source>
        <translation>无线网卡: %1 可用</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/network-tray.cpp" line="524"/>
        <source>Wired network card: %1 unavailable</source>
        <translation>有线网卡: %1 不可用</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/network-tray.cpp" line="529"/>
        <source>Wireless network card: %1 unavailable</source>
        <translation>无线网卡: %1 不可用</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/network-tray.h" line="115"/>
        <location filename="../plugins/network/src/tray/network-tray.h" line="123"/>
        <source>Network connected</source>
        <translation>网络已连接</translation>
    </message>
</context>
<context>
    <name>OutputPage</name>
    <message>
        <location filename="../plugins/audio/src/plugin/output-page.ui" line="14"/>
        <source>OutputPage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/output-page.ui" line="40"/>
        <source>Output cards</source>
        <translation>输出声卡</translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/output-page.ui" line="73"/>
        <source>Output devices</source>
        <translation>输出设备</translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/output-page.ui" line="102"/>
        <source>ComboBoxOutputDevices</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/output-page.ui" line="125"/>
        <source>Output volume</source>
        <translation>输出音量</translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/output-page.ui" line="182"/>
        <source>SlilderVolumeSetting</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/output-page.ui" line="208"/>
        <source>Left/right balance</source>
        <translation>左/右平衡</translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/output-page.ui" line="246"/>
        <source>SliderVolumeBalance</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/output-page.ui" line="260"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/output-page.ui" line="286"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/plugin/output-page.cpp" line="283"/>
        <source>No output device detected</source>
        <translation>未检测到输出设备</translation>
    </message>
</context>
<context>
    <name>PanelWindow</name>
    <message>
        <location filename="../src/panel-window.cpp" line="46"/>
        <source>Control Panel</source>
        <translation>控制面板</translation>
    </message>
</context>
<context>
    <name>PasswordExpirationPolicyPage</name>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="14"/>
        <source>PasswordExpirationPolicyPage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="43"/>
        <source>User expires</source>
        <translation>用户过期时间</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="67"/>
        <source>SpinBoxUserExpires</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="70"/>
        <source>yyyy-MM-dd</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="87"/>
        <source>Last password change</source>
        <translation>最近一次密码修改时间</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="111"/>
        <source>LabelLastPasswdChange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="114"/>
        <source>1990-01-01</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="131"/>
        <source>Maximum vaild days of password</source>
        <translation>密码最大有限天数</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="155"/>
        <source>SpinBoxMaximumValidDays</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="172"/>
        <source>Prompt time before password expiration</source>
        <translation>密码过期之前提醒的天数</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="196"/>
        <source>SpinBoxPromptBeforeExpiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="213"/>
        <source>how many days after password expires will become inactive</source>
        <translation>密码过期多少天认定为失效</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="237"/>
        <source>SpinBoxPasswdInactiveTime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="298"/>
        <source>ButtonSave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="301"/>
        <source>save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="342"/>
        <source>ButtonReturn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.ui" line="345"/>
        <source>return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.cpp" line="73"/>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.cpp" line="80"/>
        <location filename="../plugins/account/pages/passwd-expiration-policy/password-expiration-policy-page.cpp" line="88"/>
        <source>day</source>
        <translation>日</translation>
    </message>
</context>
<context>
    <name>PluginConnectionList</name>
    <message>
        <location filename="../plugins/network/src/plugin/plugin-connection-list.cpp" line="181"/>
        <source>Other WiFi networks</source>
        <translation>其它WIFI网络</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/plugin-connection-list.cpp" line="289"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/plugin-connection-list.cpp" line="290"/>
        <source>Please input a network name</source>
        <translation>请输入网络名称</translation>
    </message>
</context>
<context>
    <name>Popup</name>
    <message>
        <source>cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>PowerPlugin</name>
    <message>
        <location filename="../plugins/power/power-plugin.cpp" line="55"/>
        <source>General Settings</source>
        <translation>通用设置</translation>
    </message>
    <message>
        <location filename="../plugins/power/power-plugin.cpp" line="64"/>
        <source>Power Settings</source>
        <translation>电源设置</translation>
    </message>
    <message>
        <location filename="../plugins/power/power-plugin.cpp" line="79"/>
        <source>Battery Settings</source>
        <translation>电池设置</translation>
    </message>
</context>
<context>
    <name>PowerProfilesWrapper</name>
    <message>
        <location filename="../plugins/power/dbus/power-profiles-wrapper.cpp" line="97"/>
        <location filename="../plugins/power/dbus/power-profiles-wrapper.cpp" line="113"/>
        <source>power-saver</source>
        <translation>省电模式</translation>
    </message>
    <message>
        <location filename="../plugins/power/dbus/power-profiles-wrapper.cpp" line="98"/>
        <location filename="../plugins/power/dbus/power-profiles-wrapper.cpp" line="114"/>
        <source>balanced</source>
        <translation>平衡模式</translation>
    </message>
    <message>
        <location filename="../plugins/power/dbus/power-profiles-wrapper.cpp" line="99"/>
        <location filename="../plugins/power/dbus/power-profiles-wrapper.cpp" line="115"/>
        <source>performance</source>
        <translation>性能模式</translation>
    </message>
</context>
<context>
    <name>PowerSettingsPage</name>
    <message>
        <location filename="../plugins/power/pages/power-settings-page.ui" line="14"/>
        <source>PowerSettingsPage</source>
        <translation>电源设置页面</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/power-settings-page.ui" line="43"/>
        <source>After idle for more than the following time, the computer will execute</source>
        <translation>空闲超过以下时间后，计算机将执行</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/power-settings-page.ui" line="60"/>
        <source>ComboIdleTime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/power-settings-page.ui" line="67"/>
        <source>ComboIdleAction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/power-settings-page.ui" line="84"/>
        <source>The monitor will turn off when it is idle</source>
        <translation>显示器空闲以下时间关闭</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/power-settings-page.ui" line="101"/>
        <source>ComboMonitorTrunOffIdleTime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/power-settings-page.cpp" line="62"/>
        <source>Suspend</source>
        <translation>待机</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/power-settings-page.cpp" line="63"/>
        <source>Shutdown</source>
        <translation>关机</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/power-settings-page.cpp" line="64"/>
        <source>Hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../plugins/power/pages/power-settings-page.cpp" line="65"/>
        <source>Do nothing</source>
        <translation>不执行操作</translation>
    </message>
</context>
<context>
    <name>PowerSubItem</name>
    <message>
        <source>Power Settings</source>
        <translation type="vanished">电源设置</translation>
    </message>
</context>
<context>
    <name>PrefsPage</name>
    <message>
        <location filename="../plugins/authentication/pages/prefs-page.cpp" line="64"/>
        <source>Authentication type Enabled status</source>
        <translation>认证类型启用状态</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/prefs-page.cpp" line="75"/>
        <location filename="../plugins/authentication/pages/prefs-page.cpp" line="178"/>
        <source>fingerprint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/prefs-page.cpp" line="76"/>
        <location filename="../plugins/authentication/pages/prefs-page.cpp" line="178"/>
        <source>fingervein</source>
        <translation>指静脉</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/prefs-page.cpp" line="115"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/prefs-page.cpp" line="130"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/prefs-page.cpp" line="138"/>
        <source>login</source>
        <translation>登录</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/prefs-page.cpp" line="139"/>
        <source>unlock</source>
        <translation>解锁</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/prefs-page.cpp" line="140"/>
        <source>empowerment</source>
        <translation>授权</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/prefs-page.cpp" line="179"/>
        <source>Apply the %1 authentication to the following applications</source>
        <translation>启用%1认证在以下的认证应用中</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/prefs-page.cpp" line="77"/>
        <source>ukey</source>
        <translation>UKey</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/prefs-page.cpp" line="78"/>
        <source>iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/prefs-page.cpp" line="79"/>
        <source>face</source>
        <translation>人脸</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Did not reply within the specified timeout</source>
        <translation type="vanished">连接超时</translation>
    </message>
    <message>
        <source>The called service is not known</source>
        <translation type="vanished">无法连接到Dbus服务</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>Open qss file failed</source>
        <translation type="vanished">加载qss文件失败</translation>
    </message>
    <message>
        <location filename="../plugins/power/power-utils.cpp" line="32"/>
        <source>%1Day</source>
        <translation>%1天</translation>
    </message>
    <message>
        <location filename="../plugins/power/power-utils.cpp" line="36"/>
        <source>%1Hour</source>
        <translation>%1小时</translation>
    </message>
    <message>
        <location filename="../plugins/power/power-utils.cpp" line="40"/>
        <source>%1Minute</source>
        <translation>%1分钟</translation>
    </message>
    <message>
        <location filename="../plugins/power/power-utils.cpp" line="44"/>
        <source>never</source>
        <translation>从不</translation>
    </message>
    <message>
        <source>SLow</source>
        <translation type="vanished">低速</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">标准</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">快速</translation>
    </message>
    <message>
        <source>Faild</source>
        <translation type="vanished">失败</translation>
    </message>
    <message>
        <source>Connect Mouse or TouchPad Dbus Failed!</source>
        <translation type="vanished">连接鼠标或触摸板Dbus服务失败！</translation>
    </message>
    <message>
        <source>Load qss file failed!</source>
        <translation type="vanished">加载qss文件失败!</translation>
    </message>
    <message>
        <location filename="../plugins/keyboard/utils/layout-list.cpp" line="142"/>
        <source>No search results, please search again...</source>
        <translation>无搜索结果，请重新搜索...</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-config.cpp" line="466"/>
        <location filename="../plugins/display/src/display-config.h" line="134"/>
        <location filename="../plugins/display/src/display-page.cpp" line="400"/>
        <location filename="../plugins/display/src/display-page.cpp" line="418"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-config.cpp" line="469"/>
        <location filename="../plugins/display/src/display-config.h" line="137"/>
        <location filename="../plugins/display/src/display-page.cpp" line="403"/>
        <location filename="../plugins/display/src/display-page.cpp" line="421"/>
        <source>OK(K)</source>
        <translation>确定(K)</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.cpp" line="407"/>
        <source>Failed to apply display settings!%1</source>
        <translation>应用显示设置失败!%1</translation>
    </message>
    <message>
        <location filename="../plugins/display/src/display-page.cpp" line="425"/>
        <source>Fallback display setting failed! %1</source>
        <translation>回撤显示设置失败! %1</translation>
    </message>
    <message>
        <source>Failed</source>
        <translation type="vanished">失败</translation>
    </message>
    <message>
        <source>Set font  failed!</source>
        <translation type="vanished">设置字体失败!</translation>
    </message>
    <message>
        <source>Get icon themes failed!</source>
        <translation type="vanished">获取图标主题失败!</translation>
    </message>
    <message>
        <source>Get cursor themes failed!</source>
        <translation type="vanished">获取光标主题失败!</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">警告</translation>
    </message>
    <message>
        <source>There is no theme to set!</source>
        <translation type="vanished">目前没有主题可以设置!</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/theme/icon/icon-theme-page.h" line="24"/>
        <source>Spring</source>
        <translation>初春</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/theme/icon/icon-theme-page.h" line="25"/>
        <source>Summer</source>
        <translation>盛夏</translation>
    </message>
</context>
<context>
    <name>SearchEdit</name>
    <message>
        <location filename="../src/search-edit/search-edit.cpp" line="45"/>
        <source>Enter keywords to search</source>
        <translation>输入关键词进行搜索</translation>
    </message>
    <message>
        <location filename="../src/search-edit/search-edit.cpp" line="84"/>
        <source>Info</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../src/search-edit/search-edit.cpp" line="84"/>
        <source>Failed to find related items, please re-enter!</source>
        <translation>未能搜索到相关项，请重新输入!</translation>
    </message>
</context>
<context>
    <name>SelectAvatarPage</name>
    <message>
        <location filename="../plugins/account/pages/select-avatar-page/select-avatar-page.cpp" line="148"/>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/select-avatar-page/select-avatar-page.cpp" line="162"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/select-avatar-page/select-avatar-page.cpp" line="179"/>
        <source>select picture</source>
        <translation>选择图片</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/select-avatar-page/select-avatar-page.cpp" line="181"/>
        <source>image files(*.bmp *.jpg *.png *.tif *.gif *.pcx *.tga *.exif *.fpx *.svg *.psd *.cdr *.pcd *.dxf *.ufo *.eps *.ai *.raw *.WMF *.webp)</source>
        <translation>图片文件(*.bmp *.jpg *.png *.tif *.gif *.pcx *.tga *.exif *.fpx *.svg *.psd *.cdr *.pcd *.dxf *.ufo *.eps *.ai *.raw *.WMF *.webp)</translation>
    </message>
</context>
<context>
    <name>SettingBriefWidget</name>
    <message>
        <location filename="../lib/common-widgets/setting-brief-widget/setting-brief-widget.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/common-widgets/setting-brief-widget/setting-brief-widget.ui" line="47"/>
        <location filename="../lib/common-widgets/setting-brief-widget/setting-brief-widget.ui" line="67"/>
        <location filename="../lib/common-widgets/setting-brief-widget/setting-brief-widget.ui" line="74"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Shortcut</name>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="78"/>
        <source>EditSearch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="178"/>
        <source>Custom</source>
        <translation>自定义</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="210"/>
        <location filename="../plugins/keybinding/shortcut.cpp" line="163"/>
        <location filename="../plugins/keybinding/shortcut.cpp" line="550"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="343"/>
        <source>ButtonAddShortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="346"/>
        <location filename="../plugins/keybinding/shortcut.ui" line="551"/>
        <location filename="../plugins/keybinding/shortcut.cpp" line="106"/>
        <location filename="../plugins/keybinding/shortcut.cpp" line="118"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="381"/>
        <source>ButtonReset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="384"/>
        <source>Reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="430"/>
        <source>Custom Shortcut Name</source>
        <translation>自定义快捷键名称</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="449"/>
        <source>EditCustomShortcutName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="463"/>
        <source>Custom Shortcut application</source>
        <translation>自定义快捷键应用程序</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="482"/>
        <location filename="../plugins/keybinding/shortcut.ui" line="700"/>
        <source>EditShortcutApp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="496"/>
        <source>Custom Shortcut Key</source>
        <translation>自定义快捷键</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="548"/>
        <source>ButtonAdd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="586"/>
        <source>ButtonCancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="589"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="635"/>
        <source>Shortcut Name</source>
        <translation>快捷键名称</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="654"/>
        <source>EditShortcutName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="681"/>
        <source>Shortcut application</source>
        <translation>快捷键应用程序</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="715"/>
        <source>Shortcut key</source>
        <translation>快捷键</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="767"/>
        <source>ButtonSave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="770"/>
        <source>Save</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="805"/>
        <source>ButtonReturn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.ui" line="808"/>
        <source>return</source>
        <translation type="unfinished">返回</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="87"/>
        <source>Please enter a search keyword...</source>
        <translation>请输入搜索关键字...</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="99"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="127"/>
        <location filename="../plugins/keybinding/shortcut.cpp" line="134"/>
        <source>Please press the new shortcut key</source>
        <translation>请输入新快捷键</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="161"/>
        <source>Finished</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="215"/>
        <source>failed to load shortcut key data!</source>
        <translation>加载快捷键数据失败！</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="257"/>
        <source>List shortcut failed,error:%1</source>
        <translation>列出快捷键失败，错误:%1</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="300"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="301"/>
        <source>Get shortcut failed,error:</source>
        <translation>获取快捷键失败，错误：</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="379"/>
        <source>Open File</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="603"/>
        <source>System</source>
        <translation>系统</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="605"/>
        <source>Sound</source>
        <translation>声音</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="655"/>
        <location filename="../plugins/keybinding/shortcut.cpp" line="705"/>
        <location filename="../plugins/keybinding/shortcut.cpp" line="726"/>
        <location filename="../plugins/keybinding/shortcut.cpp" line="761"/>
        <location filename="../plugins/keybinding/shortcut.cpp" line="782"/>
        <location filename="../plugins/keybinding/shortcut.cpp" line="805"/>
        <location filename="../plugins/keybinding/shortcut.cpp" line="825"/>
        <source>Failed</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="656"/>
        <source>Delete shortcut failed,error:</source>
        <translation>删除快捷键失败，错误：</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="669"/>
        <location filename="../plugins/keybinding/shortcut.cpp" line="744"/>
        <source>Warning</source>
        <translation type="unfinished">警告</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="670"/>
        <location filename="../plugins/keybinding/shortcut.cpp" line="745"/>
        <source>Please complete the shortcut information!</source>
        <translation>请完善快捷键信息！</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="679"/>
        <source>Set shortcut</source>
        <translation>设置快捷键</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="680"/>
        <source>Are you sure you want to disable this shortcut?</source>
        <translation>是否确定要禁用此快捷键？</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="706"/>
        <source>Modify system shortcut failed,error:</source>
        <translation>修改系统快捷键失败，错误：</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="727"/>
        <source>Modify custom shortcut failed,error:</source>
        <translation>修改自定义快捷键失败，错误：</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="762"/>
        <source>Add custom shortcut failed,error:</source>
        <translation>添加自定义快捷键失败，错误：</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="783"/>
        <source>Reset shortcut failed,error:</source>
        <translation>重置快捷键失败，错误：</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="806"/>
        <source>Cannot use shortcut &quot;%1&quot;, Because you cannot enter with this key.Please try again using Ctrl, Alt, or Shift at the same time.</source>
        <translation>无法使用快捷键&quot;%1&quot;,因为使用此键将无法输入，请同时使用Ctrl，Alt，Shift再试一次。</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/shortcut.cpp" line="826"/>
        <source>Shortcut keys %1 are already used in %2,Please try again!</source>
        <translation>快捷键%1已用于%2，请再试一次！</translation>
    </message>
</context>
<context>
    <name>ShortcutItem</name>
    <message>
        <location filename="../plugins/keybinding/utils/shortcut-item.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/shortcut-item.ui" line="53"/>
        <location filename="../plugins/keybinding/utils/shortcut-item.ui" line="73"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ShowQRCode</name>
    <message>
        <source>Scan QR code to get machine code</source>
        <translation type="vanished">扫描二维码获取机器码</translation>
    </message>
    <message>
        <source>QRcode of Machine and Activation Code</source>
        <translation type="vanished">激活信息二维码</translation>
    </message>
    <message>
        <source>Scan QR code to get activation code</source>
        <translation type="vanished">扫描二维码获取激活码</translation>
    </message>
</context>
<context>
    <name>StatusNotification</name>
    <message>
        <location filename="../plugins/network/src/status-notification.cpp" line="27"/>
        <location filename="../plugins/network/src/status-notification.cpp" line="35"/>
        <location filename="../plugins/network/src/status-notification.cpp" line="46"/>
        <location filename="../plugins/network/src/status-notification.cpp" line="55"/>
        <location filename="../plugins/network/src/status-notification.cpp" line="63"/>
        <source>Connection Failed</source>
        <translation>连接失败</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/status-notification.cpp" line="28"/>
        <source>the network not found</source>
        <translation>未找到网络</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/status-notification.cpp" line="36"/>
        <source>The hidden network &quot;%1&quot; to be connected has been detected and exists in the network list</source>
        <translation>要连接的隐藏网络“%1”已经被探测到，并存在于网络列表中</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/status-notification.cpp" line="47"/>
        <location filename="../plugins/network/src/status-notification.cpp" line="56"/>
        <source>Failed to connect to the network &quot;%1&quot;</source>
        <translation>无法连接到网络 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/status-notification.cpp" line="71"/>
        <source>Connection activated</source>
        <translation>网络已连接</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/status-notification.cpp" line="72"/>
        <source>You are now connected to the network &quot;%1&quot;</source>
        <translation>您已连接到网络 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/status-notification.cpp" line="81"/>
        <source>Connection deactivated</source>
        <translation>连接断开</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/status-notification.cpp" line="82"/>
        <source>You have now disconnected the network &quot;%1&quot;</source>
        <translation>您已断开网络连接 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/status-notification.cpp" line="90"/>
        <source>Connection deleted</source>
        <translation>连接已删除</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/status-notification.cpp" line="91"/>
        <source>The connection has been deleted &quot;%1&quot;</source>
        <translation>已删除连接 &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>SystemInfoSubItem</name>
    <message>
        <location filename="../plugins/system/system-subitem.h" line="33"/>
        <source>System Information</source>
        <translation>系统信息</translation>
    </message>
</context>
<context>
    <name>SystemInformation</name>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="152"/>
        <source>Host Name:</source>
        <translation>主机名:</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="178"/>
        <source>LabelHostName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="181"/>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="278"/>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="356"/>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="422"/>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="485"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="212"/>
        <source>ButtonChangeHostName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="215"/>
        <source>Change</source>
        <translation>更改</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="255"/>
        <source>System Version:</source>
        <translation>系统版本:</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="275"/>
        <source>LabelSystemVersion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="333"/>
        <source>Kernel Version:</source>
        <translation>内核版本:</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="353"/>
        <source>LabelKernelVersion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="399"/>
        <source>System Architecture:</source>
        <translation>系统架构:</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="419"/>
        <source>LabelSystemArch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="465"/>
        <source>Activation status:</source>
        <translation>激活状态:</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="510"/>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="585"/>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="660"/>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="732"/>
        <source>Show</source>
        <translation>查看</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="550"/>
        <source>EULA:</source>
        <translation>最终用户许可协议:</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="582"/>
        <source>ButtonShowEULA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="625"/>
        <source>Version License:</source>
        <translation>版本协议:</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="657"/>
        <source>ButtonShowVersionLicense</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.cpp" line="108"/>
        <location filename="../plugins/system/pages/system-information/system-information.cpp" line="109"/>
        <location filename="../plugins/system/pages/system-information/system-information.cpp" line="110"/>
        <location filename="../plugins/system/pages/system-information/system-information.cpp" line="111"/>
        <source>Unknow</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.cpp" line="246"/>
        <source>UnActivated</source>
        <translation>未激活</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.cpp" line="255"/>
        <source>Activation code has expired</source>
        <translation>激活码已过期</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.cpp" line="260"/>
        <source>Permanently activated</source>
        <translation>永久激活</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.cpp" line="265"/>
        <source>Activated</source>
        <translation>已激活</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.cpp" line="308"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.cpp" line="308"/>
        <source>Failed to open the license activator</source>
        <translation>启动激活许可证弹窗失败</translation>
    </message>
    <message>
        <source>Copyright ©</source>
        <translation type="vanished">版权所有 ©</translation>
    </message>
    <message>
        <source>KylinSec. All rights reserved.</source>
        <translation type="vanished">KylinSec.保留所有权利.</translation>
    </message>
    <message>
        <location filename="../plugins/system/pages/system-information/system-information.ui" line="700"/>
        <source>Privacy policy:</source>
        <translation>隐私协议：</translation>
    </message>
</context>
<context>
    <name>SystemInformationWidget</name>
    <message>
        <source>Host Name:</source>
        <translation type="vanished">主机名:</translation>
    </message>
    <message>
        <source>System Version:</source>
        <translation type="vanished">系统版本:</translation>
    </message>
    <message>
        <source>Kernel Version:</source>
        <translation type="vanished">内核版本:</translation>
    </message>
    <message>
        <source>System Architecture:</source>
        <translation type="vanished">系统架构:</translation>
    </message>
    <message>
        <source>Installation time:</source>
        <translation type="vanished">安装时间:</translation>
    </message>
    <message>
        <source>Activation status:</source>
        <translation type="vanished">激活状态:</translation>
    </message>
    <message>
        <source>Expiry date:</source>
        <translation type="vanished">质保期:</translation>
    </message>
    <message>
        <source>EULA:</source>
        <translation type="vanished">最终用户许可协议:</translation>
    </message>
    <message>
        <source>Version License:</source>
        <translation type="vanished">版本协议:</translation>
    </message>
    <message>
        <source>Contact Us:</source>
        <translation type="vanished">联系我们:</translation>
    </message>
    <message>
        <source>Change</source>
        <translation type="vanished">更改</translation>
    </message>
    <message>
        <source>Show</source>
        <translation type="vanished">查看</translation>
    </message>
    <message>
        <source>Unknow</source>
        <translation type="vanished">未知</translation>
    </message>
    <message>
        <source>The current time is illegal</source>
        <translation type="vanished">当前时间不合法</translation>
    </message>
    <message>
        <source>Less than the installation time</source>
        <translation type="vanished">小于安装时间</translation>
    </message>
    <message>
        <source>Not activated. Trail expiration: </source>
        <translation type="vanished">未激活.试用到期: </translation>
    </message>
    <message>
        <source>Can&apos;t get activation information</source>
        <translation type="vanished">无法获取激活信息</translation>
    </message>
    <message>
        <source>Activate</source>
        <translation type="vanished">激活</translation>
    </message>
    <message>
        <source>get service status failed</source>
        <translation type="vanished">获取服务状态信息失败</translation>
    </message>
    <message>
        <source>Not yet</source>
        <translation type="vanished">暂无</translation>
    </message>
    <message>
        <source>Activated</source>
        <translation type="vanished">已激活</translation>
    </message>
    <message>
        <source>Forever</source>
        <translation type="vanished">永久授权</translation>
    </message>
    <message>
        <source>Copyright ©</source>
        <translation type="vanished">版权所有 ©</translation>
    </message>
    <message>
        <source>KylinSec. All rights reserved.</source>
        <translation type="vanished">KylinSec.保留所有权利.</translation>
    </message>
</context>
<context>
    <name>TextInputDialog</name>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/text-input-dialog.cpp" line="40"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/text-input-dialog.cpp" line="43"/>
        <source>Yes</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/text-input-dialog.cpp" line="44"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>ThemePage</name>
    <message>
        <location filename="../plugins/appearance/pages/theme/theme-page.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/theme/theme-page.ui" line="81"/>
        <source>Dark and Light Theme</source>
        <translation>深浅色主题设置</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/theme/theme-page.ui" line="109"/>
        <source>Themes Settings</source>
        <translation>主题设置</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/theme/theme-page.ui" line="149"/>
        <source>Open Window Effects</source>
        <translation>打开或关闭窗口特效</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/theme/theme-page.cpp" line="88"/>
        <location filename="../plugins/appearance/pages/theme/theme-page.cpp" line="120"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/theme/theme-page.cpp" line="151"/>
        <source>Light Theme</source>
        <translation>浅色</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/theme/theme-page.cpp" line="152"/>
        <source>Auto</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/theme/theme-page.cpp" line="153"/>
        <source>Dark Theme</source>
        <translation>深色</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/theme/theme-page.cpp" line="82"/>
        <source>Choose icon Theme</source>
        <translation>选择图标主题</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/theme/theme-page.cpp" line="114"/>
        <source>Choose cursor Themes</source>
        <translation>选择光标主题</translation>
    </message>
</context>
<context>
    <name>ThemeWidget</name>
    <message>
        <source>Dark Theme</source>
        <translation type="vanished">深色</translation>
    </message>
    <message>
        <source>Light Theme</source>
        <translation type="vanished">浅色</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="vanished">自动</translation>
    </message>
</context>
<context>
    <name>Themes</name>
    <message>
        <source>Dark and Light Theme</source>
        <translation type="vanished">深浅色主题设置</translation>
    </message>
    <message>
        <source>Themes Settings</source>
        <translation type="vanished">主题设置</translation>
    </message>
    <message>
        <source>Open Window Effects</source>
        <translation type="vanished">打开或关闭窗口特效</translation>
    </message>
    <message>
        <source>Choose icon themes</source>
        <translation type="vanished">选择图标主题</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">未知</translation>
    </message>
    <message>
        <source>Choose cursor themes</source>
        <translation type="vanished">选择光标主题</translation>
    </message>
</context>
<context>
    <name>ThreadObject</name>
    <message>
        <location filename="../plugins/keybinding/utils/thread-object.cpp" line="137"/>
        <source>Failed</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="../plugins/keybinding/utils/thread-object.cpp" line="138"/>
        <source>List shortcut failed,error:</source>
        <translation>列出快捷键失败，错误：</translation>
    </message>
</context>
<context>
    <name>TimeDateSubItem</name>
    <message>
        <location filename="../plugins/timedate/timedate-subitem.cpp" line="44"/>
        <source>Time Date Settings</source>
        <translation>日期时间设置</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/timedate-subitem.cpp" line="76"/>
        <source>Chnage time Zone</source>
        <translation>更改时区</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/timedate-subitem.cpp" line="77"/>
        <source>Set time Manually</source>
        <translation>手动设置时间</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/timedate-subitem.cpp" line="78"/>
        <source>Time date format setting</source>
        <translation>日期时间格式设置</translation>
    </message>
</context>
<context>
    <name>TimezoneSettings</name>
    <message>
        <location filename="../plugins/timedate/pages/timezone-settings/time-zone-settings.ui" line="14"/>
        <source>TimezoneSettings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/timezone-settings/time-zone-settings.ui" line="52"/>
        <source>Select Time Zone</source>
        <translation>选择时区</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/timezone-settings/time-zone-settings.ui" line="116"/>
        <source>ButtonSave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/timezone-settings/time-zone-settings.ui" line="119"/>
        <source>save</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/timezone-settings/time-zone-settings.ui" line="160"/>
        <source>ButtonReturn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/timedate/pages/timezone-settings/time-zone-settings.ui" line="163"/>
        <source>reset</source>
        <translation>重置</translation>
    </message>
</context>
<context>
    <name>TopBar</name>
    <message>
        <location filename="../lib/common-widgets/kiran-collapse/top-bar.ui" line="20"/>
        <source>ListExpansionSpace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/common-widgets/kiran-collapse/top-bar.ui" line="43"/>
        <source>TITLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/common-widgets/kiran-collapse/top-bar.ui" line="78"/>
        <source>FLAG</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TouchPadPage</name>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="86"/>
        <source>TouchPad Enabled</source>
        <translation>开启触摸板</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="106"/>
        <source>SwitchTouchPadEnable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="137"/>
        <source>Select TouchPad Hand</source>
        <translation>选择触摸板使用模式</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="156"/>
        <source>ComboTouchPadHand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="189"/>
        <source>TouchPad Motion Acceleration</source>
        <translation>触摸板移动加速</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="211"/>
        <source>SliderTouchPadMotionAcceleration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="235"/>
        <source>Slow</source>
        <translation>慢</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="255"/>
        <source>Fast</source>
        <translation>快</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="285"/>
        <source>Select Click Method</source>
        <translation>设置点击触摸板方式</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="304"/>
        <source>ComboClickMethod</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="332"/>
        <source>Select Scroll Method</source>
        <translation>滚动窗口方式</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="351"/>
        <source>ComboScrollMethod</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="379"/>
        <source>Natural Scroll</source>
        <translation>是否为自然滚动</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="399"/>
        <source>ComboNaturalScroll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="430"/>
        <source>Enabled while Typing</source>
        <translation>打字时触摸板禁用</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="450"/>
        <source>SwitchTypingEnable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="481"/>
        <source>Tap to Click</source>
        <translation>轻击(不按下)触摸板功能是否生效</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.ui" line="501"/>
        <source>SwtichTapToClick</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.cpp" line="268"/>
        <source>Right Hand Mode</source>
        <translation>右手模式</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.cpp" line="268"/>
        <source>Left Hand Mode</source>
        <translation>左手模式</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.cpp" line="272"/>
        <source>Press and Tap</source>
        <translation>按键和轻触</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.cpp" line="272"/>
        <source>Tap</source>
        <translation>轻触</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.cpp" line="276"/>
        <source>Two Finger Scroll</source>
        <translation>两指滑动</translation>
    </message>
    <message>
        <location filename="../plugins/mouse/pages/touchpad-page.cpp" line="276"/>
        <source>Edge Scroll</source>
        <translation>边缘滑动</translation>
    </message>
</context>
<context>
    <name>TouchPadSettings</name>
    <message>
        <source>Touchpad Enabled</source>
        <translation type="vanished">禁用触摸板</translation>
    </message>
    <message>
        <source>Disable TouchPad</source>
        <translation type="vanished">禁用触摸板</translation>
    </message>
    <message>
        <source>TouchPad Enabled</source>
        <translation type="vanished">开启触摸板</translation>
    </message>
    <message>
        <source>Select TouchPad Hand</source>
        <translation type="vanished">选择触摸板使用模式</translation>
    </message>
    <message>
        <source>TouchPad Motion Acceleration</source>
        <translation type="vanished">触摸板移动加速</translation>
    </message>
    <message>
        <source>Select Click Method</source>
        <translation type="vanished">设置点击触摸板方式</translation>
    </message>
    <message>
        <source>Select Scroll Method</source>
        <translation type="vanished">滚动窗口方式</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">是否为自然滚动</translation>
    </message>
    <message>
        <source>Enabled while Typing</source>
        <translation type="vanished">打字时触摸板禁用</translation>
    </message>
    <message>
        <source>Tap to Click</source>
        <translation type="vanished">轻击(不按下)触摸板功能是否生效</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">慢</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">标准</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">快</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">右手模式</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">左手模式</translation>
    </message>
    <message>
        <source>Press and Tap</source>
        <translation type="vanished">按键和轻触</translation>
    </message>
    <message>
        <source>Tap</source>
        <translation type="vanished">轻触</translation>
    </message>
    <message>
        <source>Two Finger Scroll</source>
        <translation type="vanished">两指滑动</translation>
    </message>
    <message>
        <source>Edge Scroll</source>
        <translation type="vanished">边缘滑动</translation>
    </message>
</context>
<context>
    <name>TouchPadSubItem</name>
    <message>
        <location filename="../plugins/mouse/touchpad-subitem.h" line="46"/>
        <source>TouchPad Settings</source>
        <translation>触摸板设置</translation>
    </message>
</context>
<context>
    <name>TrayConnectionList</name>
    <message>
        <location filename="../plugins/network/src/tray/tray-connection-list.cpp" line="186"/>
        <source>Other WiFi networks</source>
        <translation>其它WIFI网络</translation>
    </message>
</context>
<context>
    <name>TrayItemWidget</name>
    <message>
        <location filename="../plugins/network/src/tray/tray-itemwidget.ui" line="32"/>
        <source>TrayItemWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/tray-itemwidget.ui" line="92"/>
        <source>Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/tray-itemwidget.ui" line="99"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/tray-itemwidget.ui" line="187"/>
        <source>Status</source>
        <translation>状态</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/tray-itemwidget.ui" line="263"/>
        <source>Ignore</source>
        <translation>忽略</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/tray-itemwidget.ui" line="288"/>
        <source>Disconnect</source>
        <translation>断开</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/tray-itemwidget.ui" line="355"/>
        <location filename="../plugins/network/src/tray/tray-itemwidget.ui" line="477"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/tray-itemwidget.ui" line="380"/>
        <location filename="../plugins/network/src/tray/tray-itemwidget.ui" line="502"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/tray-itemwidget.cpp" line="147"/>
        <source>Connected</source>
        <translation>已连接</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/tray-itemwidget.cpp" line="158"/>
        <source>Unconnected</source>
        <translation>未连接</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/tray-itemwidget.cpp" line="192"/>
        <source>Please input password</source>
        <translation>请输入密码</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/tray-itemwidget.cpp" line="201"/>
        <source>Please input a network name</source>
        <translation>请输入网络名称</translation>
    </message>
</context>
<context>
    <name>TrayPage</name>
    <message>
        <location filename="../plugins/network/src/tray/tray-page.ui" line="32"/>
        <source>TrayPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/tray-page.ui" line="98"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/tray-page.cpp" line="109"/>
        <source>Select wired network card</source>
        <translation>请选择有线网卡</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/tray/tray-page.cpp" line="114"/>
        <source>Select wireless network card</source>
        <translation>请选择无线网卡</translation>
    </message>
</context>
<context>
    <name>UKeyPage</name>
    <message>
        <location filename="../plugins/authentication/pages/ukey-page.cpp" line="52"/>
        <source>Ukey</source>
        <translation>UKey</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/ukey-page.cpp" line="53"/>
        <source>Default Ukey device</source>
        <translation>默认UKey设备</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/ukey-page.cpp" line="54"/>
        <source>List of devices bound to the Ukey</source>
        <translation>绑定UKey设备列表</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/ukey-page.cpp" line="74"/>
        <location filename="../plugins/authentication/pages/ukey-page.cpp" line="88"/>
        <location filename="../plugins/authentication/pages/ukey-page.cpp" line="126"/>
        <source>error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/ukey-page.cpp" line="89"/>
        <source>No UKey device detected, pelease insert the UKey device and perform operations</source>
        <translation>未检测到UKey设备，请插入UKey设备再次执行操作</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/ukey-page.cpp" line="95"/>
        <source>UKey Enroll</source>
        <translation type="unfinished">UKey录入</translation>
    </message>
    <message>
        <location filename="../plugins/authentication/pages/ukey-page.cpp" line="96"/>
        <source>Please enter the ukey pin code</source>
        <translation type="unfinished">请输入UKey PIN码</translation>
    </message>
</context>
<context>
    <name>UKeyPinCodeDialog</name>
    <message>
        <source>UKey Enroll</source>
        <translation type="vanished">UKey录入</translation>
    </message>
    <message>
        <source>Please enter the ukey pin code</source>
        <translation type="vanished">请输入UKey PIN码</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>UserInfoPage</name>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="138"/>
        <source>Account</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="167"/>
        <source>Change password</source>
        <translation>修改密码</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="193"/>
        <source>User id</source>
        <translation>用户ID</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="251"/>
        <source>User type</source>
        <translation>用户类型</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="312"/>
        <source>User status</source>
        <translation>启用用户</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="367"/>
        <source>auth manager</source>
        <translation>认证管理</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="396"/>
        <source>Password expiration policy</source>
        <translation>密码过期策略</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="461"/>
        <source>Confirm</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="502"/>
        <source>Delete</source>
        <translation>删除用户</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="577"/>
        <source>Current password</source>
        <translation>当前密码</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="623"/>
        <source>EditCurrentPasswd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="641"/>
        <source>New password</source>
        <translation>新密码</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="676"/>
        <source>EditNewPasswd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="693"/>
        <source>Enter the new password again</source>
        <translation>再次输入新密码</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="728"/>
        <source>EditNewPasswdAgain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="789"/>
        <source>EditPasswdSave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="792"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="833"/>
        <source>EditPasswdCancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.ui" line="836"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Account type</source>
        <translation type="vanished">帐户类型</translation>
    </message>
    <message>
        <source>Account status</source>
        <translation type="vanished">启用帐户</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="143"/>
        <source>standard</source>
        <translation>普通用户</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="144"/>
        <source>administrator</source>
        <translation>管理员</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="222"/>
        <source>Please enter the new user password</source>
        <translation>请输入新密码</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="230"/>
        <source>Please enter the password again</source>
        <translation>请再次输入密码</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="236"/>
        <source>The password you enter must be the same as the former one</source>
        <translation>两次密码不相同，请核对后，再次输入</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="248"/>
        <source>Please enter the current user password</source>
        <translation>请输入当前密码</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="254"/>
        <source>The current password is incorrect</source>
        <translation>当前密码错误，请再次输入</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="261"/>
        <source>The new password cannot be the same as the current password</source>
        <translation>新密码不能和旧密码相同，请重新输入</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="267"/>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="275"/>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="313"/>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="332"/>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="366"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="267"/>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="275"/>
        <source>Password encryption failed</source>
        <translation>密码加密失败</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="318"/>
        <source>user information updated successfully</source>
        <translation>用户信息更新成功</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="338"/>
        <source>Password updated successfully</source>
        <translation>密码更新成功</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="344"/>
        <source>The directory and files under the user&apos;s home directory are deleted with the user.Are you sure you want to delete the user(%1)?</source>
        <translation>用户目录下的目录和文件会随用户一起删除，确定要删除%1用户吗？</translation>
    </message>
    <message>
        <location filename="../plugins/account/pages/user-info-page/user-info-page.cpp" line="347"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <source>Account information updated successfully</source>
        <translation type="vanished">帐户信息更新成功</translation>
    </message>
</context>
<context>
    <name>UserlicenseAgreement</name>
    <message>
        <source>Export</source>
        <translation type="vanished">导出</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>Export EULA</source>
        <translation type="vanished">导出最终用户许可协议</translation>
    </message>
    <message>
        <source>Export EULA failed!</source>
        <translation type="vanished">导出最终用户许可协议失败!</translation>
    </message>
    <message>
        <source>User End License Agreement</source>
        <translation type="vanished">最终用户许可协议</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">暂无</translation>
    </message>
</context>
<context>
    <name>VolumeIntputSubItem</name>
    <message>
        <location filename="../plugins/audio/src/plugin/volume-input-subitem.h" line="32"/>
        <source>VolumeInput</source>
        <translation>输入</translation>
    </message>
</context>
<context>
    <name>VolumeOutputSubItem</name>
    <message>
        <location filename="../plugins/audio/src/plugin/volume-output-subitem.h" line="32"/>
        <source>VolumeOutput</source>
        <translation>输出</translation>
    </message>
</context>
<context>
    <name>VolumeSettingPage</name>
    <message>
        <location filename="../plugins/audio/src/system-tray/volume-setting-page.ui" line="35"/>
        <source>VolumeSettingPage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/audio/src/system-tray/volume-setting-page.ui" line="101"/>
        <location filename="../plugins/audio/src/system-tray/volume-setting-page.cpp" line="96"/>
        <source>Volume</source>
        <translation>音量</translation>
    </message>
</context>
<context>
    <name>VpnIPsec</name>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="20"/>
        <source>VpnIPsec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="43"/>
        <source>Enable IPsec</source>
        <translation>启用IPsec</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="88"/>
        <source>Group Name</source>
        <translation>组名</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="107"/>
        <source>EditGroupName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="121"/>
        <source>Group ID</source>
        <translation>组ID</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="140"/>
        <source>EditGroupId</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="154"/>
        <source>Pre-Shared Key</source>
        <translation>预共享密钥</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="175"/>
        <source>EditPreSharedKey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="182"/>
        <source>Show Password</source>
        <translation>显示密码</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="198"/>
        <source>Internet Key Exchange Protocol</source>
        <translation>密钥交换协议</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="217"/>
        <source>EditIpsecIKE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="231"/>
        <source>Encapsulating Security Payload</source>
        <translation>安全封装协议</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipsec.ui" line="250"/>
        <source>EditIpsecESP</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VpnIpvx</name>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.ui" line="14"/>
        <source>VpnIpvx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.ui" line="37"/>
        <source>IPV4 Method</source>
        <translation>IPV4方法</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.ui" line="56"/>
        <source>ComboBoxVPNIpv4Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.ui" line="67"/>
        <source>Only applied in corresponding resources</source>
        <translation>仅用于相对应的网络上的资源</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.ui" line="97"/>
        <source>Preferred DNS</source>
        <translation>首选DNS</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.ui" line="116"/>
        <source>EditVPNIpv4PreferredDNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.ui" line="133"/>
        <source>Alternate DNS</source>
        <translation>备选DNS</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.ui" line="152"/>
        <source>EditIpv4AlternateDNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ipvx.cpp" line="37"/>
        <source>Auto</source>
        <translation>自动</translation>
    </message>
</context>
<context>
    <name>VpnL2tpSetting</name>
    <message>
        <location filename="../plugins/network/src/plugin/settings/vpn/vpn-l2tp-setting.ui" line="14"/>
        <source>VpnL2tpSetting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/settings/vpn/vpn-l2tp-setting.cpp" line="27"/>
        <source>VPN name</source>
        <translation>VPN名称</translation>
    </message>
</context>
<context>
    <name>VpnManager</name>
    <message>
        <location filename="../plugins/network/src/plugin/manager/vpn-manager.ui" line="14"/>
        <location filename="../plugins/network/src/plugin/manager/vpn-manager.ui" line="17"/>
        <source>VpnManager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/manager/vpn-manager.ui" line="133"/>
        <source>VPN type</source>
        <translation>VPN类型</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/manager/vpn-manager.ui" line="244"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/manager/vpn-manager.ui" line="285"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/manager/vpn-manager.cpp" line="49"/>
        <source>VPN</source>
        <translation type="unfinished">VPN</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/manager/vpn-manager.cpp" line="52"/>
        <source>L2TP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/manager/vpn-manager.cpp" line="201"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/manager/vpn-manager.cpp" line="202"/>
        <source>Password required to connect to %1.</source>
        <translation>连接网络 &quot;%1&quot; 需要密码</translation>
    </message>
</context>
<context>
    <name>VpnPpp</name>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.ui" line="14"/>
        <source>VpnPpp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.ui" line="37"/>
        <source>Use MPPE</source>
        <translation>使用MPPE</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.ui" line="85"/>
        <source>Security</source>
        <translation>安全</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.ui" line="110"/>
        <source>ComboBoxMppeSecurity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.ui" line="121"/>
        <source>Stateful MPPE</source>
        <translation>使用带状态的MPPE</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="46"/>
        <source>All available (default)</source>
        <translation>都可用（默认）</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="47"/>
        <source>40-bit (less secure)</source>
        <translation>40位（较安全）</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="48"/>
        <source>128-bit (most secure)</source>
        <translation>128位（最安全）</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="92"/>
        <source>Refuse EAP Authentication</source>
        <translation>拒绝EAP认证</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="93"/>
        <source>Refuse PAP Authentication</source>
        <translation>拒绝PAP认证</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="94"/>
        <source>Refuse CHAP Authentication</source>
        <translation>拒绝CHAP认证</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="95"/>
        <source>Refuse MSCHAP Authentication</source>
        <translation>拒绝MSCHAP认证</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="96"/>
        <source>Refuse MSCHAPv2 Authentication</source>
        <translation>拒绝MSCHAPv2认证</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="97"/>
        <source>No BSD Data Compression</source>
        <translation>无BSD数据压缩</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="98"/>
        <source>No Deflate Data Compression</source>
        <translation>无Deflate数据压缩</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="99"/>
        <source>No TCP Header Compression</source>
        <translation>无TCP头压缩</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="100"/>
        <source>No Protocol Field Compression</source>
        <translation>无协议字段压缩</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="101"/>
        <source>No Address/Control Compression</source>
        <translation>无地址/控制压缩</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-ppp.cpp" line="102"/>
        <source>Send PPP Echo Packets</source>
        <translation>发送PPP回响包</translation>
    </message>
</context>
<context>
    <name>VpnPptpSetting</name>
    <message>
        <location filename="../plugins/network/src/plugin/settings/vpn/vpn-pptp-setting.ui" line="14"/>
        <source>VpnPptpSetting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/settings/vpn/vpn-pptp-setting.cpp" line="26"/>
        <source>VPN name</source>
        <translation>VPN名称</translation>
    </message>
</context>
<context>
    <name>VpnWidget</name>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="14"/>
        <source>VpnWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="46"/>
        <source>Gateway</source>
        <translation>网关</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="65"/>
        <source>EditVPNGateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="85"/>
        <source>User Name</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="104"/>
        <source>EditVPNUserName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="118"/>
        <source>Password Options</source>
        <translation>密码选项</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="140"/>
        <source>ComboBoxVPNPasswordOptions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="167"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="188"/>
        <source>EditVPNPassword</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="195"/>
        <source>ButtonPasswordVisual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="198"/>
        <source>Show Password</source>
        <translation>显示密码</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="212"/>
        <source>NT Domain</source>
        <translation>NT域</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.ui" line="231"/>
        <source>EditNTDomain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="29"/>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="30"/>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="31"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="41"/>
        <source>Saved</source>
        <translation>已保存的</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="42"/>
        <source>Ask</source>
        <translation>总是询问</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="43"/>
        <source>Not required</source>
        <translation>不要求</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="204"/>
        <source>Gateway can not be empty</source>
        <translation>网关不能为空</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="214"/>
        <source>Gateway invalid</source>
        <translation>无效的网关</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="224"/>
        <source>user name can not be empty</source>
        <translation>用户名不能为空</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/vpn/vpn-widget.cpp" line="234"/>
        <source>password can not be empty</source>
        <translation>密码不能为空</translation>
    </message>
</context>
<context>
    <name>Wallpaper</name>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.ui" line="68"/>
        <source>Set wallpaper</source>
        <translation>壁纸设置</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.ui" line="89"/>
        <source>FrameLockScreenPreview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.ui" line="119"/>
        <source>FrameDesktopPreivew</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.ui" line="143"/>
        <source>Desktop Wallpaper Preview</source>
        <translation>桌面壁纸预览</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.ui" line="153"/>
        <source>Lock Screen WallPaper Preview</source>
        <translation>锁屏壁纸预览</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.ui" line="172"/>
        <source>Select wallpaper</source>
        <translation>选择壁纸</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.ui" line="226"/>
        <source>Select Wallpaper</source>
        <translation>选择壁纸</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="108"/>
        <source>Set Desktop Wallpaper</source>
        <translation>选择桌面壁纸</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="124"/>
        <source>Set Lock Screen Wallpaper</source>
        <translation>选择锁屏壁纸</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="164"/>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="176"/>
        <source>set wallpaper</source>
        <translation>壁纸设置</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="164"/>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="176"/>
        <source>Set wallpaper failed!</source>
        <translation>壁纸设置失败!</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="187"/>
        <source>select picture</source>
        <translation type="unfinished">选择图片</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="189"/>
        <source>image files(*.bmp *.jpg *.png *.tif *.gif *.pcx *.tga *.exif *.fpx *.svg *.psd *.cdr *.pcd *.dxf *.ufo *.eps *.ai *.raw *.WMF *.webp)</source>
        <translation type="unfinished">图片文件(*.bmp *.jpg *.png *.tif *.gif *.pcx *.tga *.exif *.fpx *.svg *.psd *.cdr *.pcd *.dxf *.ufo *.eps *.ai *.raw *.WMF *.webp)</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="209"/>
        <source>Add Image Failed</source>
        <translation>添加壁纸失败</translation>
    </message>
    <message>
        <location filename="../plugins/appearance/pages/wallpaper/wallpaper.cpp" line="210"/>
        <source>The image already exists!</source>
        <translation>该壁纸已存在!</translation>
    </message>
</context>
<context>
    <name>WiredManager</name>
    <message>
        <location filename="../plugins/network/src/plugin/manager/wired-manager.ui" line="14"/>
        <source>WiredManager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/manager/wired-manager.ui" line="152"/>
        <source>ButtonSave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/manager/wired-manager.ui" line="155"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/manager/wired-manager.ui" line="196"/>
        <source>ButtonReturn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/manager/wired-manager.ui" line="199"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <source>Wired Network Adapter</source>
        <translation type="vanished">有线网络配置</translation>
    </message>
    <message>
        <source>The carrier is pulled out</source>
        <translation type="vanished">网线被拔出</translation>
    </message>
    <message>
        <source>The current device is not available</source>
        <translation type="vanished">当前设备不可用</translation>
    </message>
</context>
<context>
    <name>WiredSettingPage</name>
    <message>
        <location filename="../plugins/network/src/plugin/settings/wired-setting-page.ui" line="14"/>
        <source>WiredSettingPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/settings/wired-setting-page.cpp" line="77"/>
        <source>Network name</source>
        <translation>网络名称</translation>
    </message>
</context>
<context>
    <name>WirelessManager</name>
    <message>
        <location filename="../plugins/network/src/plugin/manager/wireless-manager.ui" line="14"/>
        <source>WirelessManager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/manager/wireless-manager.ui" line="149"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/manager/wireless-manager.ui" line="190"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <source>Wireless Network Adapter</source>
        <translation type="vanished">无线网卡</translation>
    </message>
    <message>
        <source>The current device is not available</source>
        <translation type="vanished">当前设备不可用</translation>
    </message>
    <message>
        <source>Tips</source>
        <translation type="vanished">提示</translation>
    </message>
    <message>
        <source>Password required to connect to %1.</source>
        <translation type="vanished">连接网络 &quot;%1&quot; 需要密码</translation>
    </message>
</context>
<context>
    <name>WirelessSecurityWidget</name>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="14"/>
        <source>WirelessSecurityWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="40"/>
        <source>Security</source>
        <translation>安全</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="59"/>
        <source>ComboBoxWirelessSecurityOption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="92"/>
        <source>Password Options</source>
        <translation>密码选项</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="111"/>
        <source>ComboBoxWirelessPasswordOption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="138"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="159"/>
        <source>EditWirelessPassword</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="166"/>
        <source>ButtonWirelessPasswordVisual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-security-widget.ui" line="169"/>
        <source>PushButton</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-security-widget.cpp" line="36"/>
        <source>None</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-security-widget.cpp" line="37"/>
        <source>WPA/WPA2 Personal</source>
        <translation>WPA/WPA2个人版</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-security-widget.cpp" line="41"/>
        <source>Save password for all users</source>
        <translation>仅为该用户存储密码</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-security-widget.cpp" line="42"/>
        <source>Save password for this user</source>
        <translation>存储所有用户密码</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-security-widget.cpp" line="43"/>
        <source>Ask me always</source>
        <translation>总是询问</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-security-widget.cpp" line="45"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
</context>
<context>
    <name>WirelessSettingPage</name>
    <message>
        <location filename="../plugins/network/src/plugin/settings/wireless-setting-page.ui" line="14"/>
        <source>WirelessSettingPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/settings/wireless-setting-page.cpp" line="67"/>
        <source>Wireless name</source>
        <translation>无线网络名称</translation>
    </message>
</context>
<context>
    <name>WirelessTrayWidget</name>
    <message>
        <location filename="../plugins/network/src/tray/wireless-tray-widget.cpp" line="492"/>
        <source>the network &quot;%1&quot; not found</source>
        <translation>未找到网络 &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>WirelessWidget</name>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-widget.ui" line="14"/>
        <source>WirelessWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-widget.ui" line="40"/>
        <source>SSID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-widget.ui" line="59"/>
        <source>EditSsid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-widget.ui" line="76"/>
        <source>MAC Address Of Device</source>
        <translation>设备MAC地址</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-widget.ui" line="95"/>
        <source>ComboBoxWirelessMacAddress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-widget.ui" line="111"/>
        <source>Custom MTU</source>
        <translation>自定义MTU</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-widget.ui" line="145"/>
        <source>SpinBoxWirelessCustomMTU</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-widget.cpp" line="38"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../plugins/network/src/plugin/setting-widget/wireless-widget.cpp" line="47"/>
        <source>No device specified</source>
        <translation>不指定设备</translation>
    </message>
</context>
<context>
    <name>YearSpinBox</name>
    <message>
        <location filename="../plugins/timedate/widgets/date-spinbox.h" line="35"/>
        <source>yyyy</source>
        <translation>yyyy年</translation>
    </message>
</context>
<context>
    <name>kiranSystemInformation</name>
    <message>
        <source>System Information</source>
        <translation type="vanished">系统信息</translation>
    </message>
    <message>
        <source>Hardware Information</source>
        <translation type="vanished">硬件信息</translation>
    </message>
    <message>
        <source>kiran-system-imformation</source>
        <translation type="vanished">系统信息</translation>
    </message>
</context>
</TS>
