<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="zh_CN">
<context>
    <name>Dialog001</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">གླེང་མོལ་</translation>
    </message>
    <message>
        <source>弹窗</source>
        <translation type="vanished">ལྡེམ་ཁུང་</translation>
    </message>
    <message>
        <source>Dialog001</source>
        <translation type="vanished">གླེང་མོལ།001</translation>
    </message>
</context>
<context>
    <name>Dialog002</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">གླེང་མོལ་</translation>
    </message>
    <message>
        <source>Dialog002</source>
        <translation type="vanished">གླེང་མོལ།002</translation>
    </message>
</context>
<context>
    <name>Popup</name>
    <message>
        <location filename="../src/subitem/popup.ui" line="14"/>
        <location filename="../build/kiran-cpanel-demo_autogen/include/ui_popup.h" line="44"/>
        <location filename="../cmake-build-debug/kiran-cpanel-demo_autogen/include/ui_popup.h" line="44"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ་</translation>
    </message>
    <message>
        <location filename="../src/subitem/popup.ui" line="26"/>
        <location filename="../build/kiran-cpanel-demo_autogen/include/ui_popup.h" line="45"/>
        <location filename="../cmake-build-debug/kiran-cpanel-demo_autogen/include/ui_popup.h" line="45"/>
        <source>Ok</source>
        <translation>ཡ་ཡ།</translation>
    </message>
    <message>
        <location filename="../src/subitem/popup.ui" line="39"/>
        <location filename="../build/kiran-cpanel-demo_autogen/include/ui_popup.h" line="46"/>
        <location filename="../cmake-build-debug/kiran-cpanel-demo_autogen/include/ui_popup.h" line="46"/>
        <source>TextLabel</source>
        <translation>ཡིག་དེབ་ཤོག་བྱང་</translation>
    </message>
    <message>
        <location filename="../src/subitem/popup.cpp" line="14"/>
        <source>cancel</source>
        <translation>མེད་པར་བཟོ་བ་</translation>
    </message>
</context>
<context>
    <name>SubItem1</name>
    <message>
        <location filename="../src/subitem/subitem-1.ui" line="14"/>
        <location filename="../build/kiran-cpanel-demo_autogen/include/ui_subitem-1.h" line="60"/>
        <location filename="../cmake-build-debug/kiran-cpanel-demo_autogen/include/ui_subitem-1.h" line="60"/>
        <source>Dialog</source>
        <translation type="unfinished">གླེང་མོལ་</translation>
    </message>
    <message>
        <location filename="../src/subitem/subitem-1.ui" line="20"/>
        <location filename="../build/kiran-cpanel-demo_autogen/include/ui_subitem-1.h" line="61"/>
        <location filename="../cmake-build-debug/kiran-cpanel-demo_autogen/include/ui_subitem-1.h" line="61"/>
        <source>弹窗</source>
        <translation type="unfinished">ལྡེམ་ཁུང་</translation>
    </message>
    <message>
        <location filename="../src/subitem/subitem-1.ui" line="27"/>
        <location filename="../build/kiran-cpanel-demo_autogen/include/ui_subitem-1.h" line="62"/>
        <location filename="../cmake-build-debug/kiran-cpanel-demo_autogen/include/ui_subitem-1.h" line="62"/>
        <source>SubItem1</source>
        <translation type="unfinished">SubItem1</translation>
    </message>
</context>
<context>
    <name>SubItem2</name>
    <message>
        <location filename="../src/subitem/subitem-2.ui" line="14"/>
        <location filename="../build/kiran-cpanel-demo_autogen/include/ui_subitem-2.h" line="53"/>
        <location filename="../cmake-build-debug/kiran-cpanel-demo_autogen/include/ui_subitem-2.h" line="53"/>
        <source>Dialog</source>
        <translation type="unfinished">གླེང་མོལ་</translation>
    </message>
    <message>
        <location filename="../src/subitem/subitem-2.ui" line="20"/>
        <location filename="../build/kiran-cpanel-demo_autogen/include/ui_subitem-2.h" line="54"/>
        <location filename="../cmake-build-debug/kiran-cpanel-demo_autogen/include/ui_subitem-2.h" line="54"/>
        <source>SubItem2</source>
        <translation type="unfinished">SubItem2</translation>
    </message>
</context>
</TS>