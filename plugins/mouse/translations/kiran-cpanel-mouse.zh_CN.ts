<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>KiranCPanelMouse</name>
    <message>
        <source>Mouse and TouchPad</source>
        <translation type="vanished">鼠标和触摸板</translation>
    </message>
</context>
<context>
    <name>KiranCPanelMouseWidget</name>
    <message>
        <source>Select Mouse Hand</source>
        <translation type="vanished">选择鼠标手持模式</translation>
    </message>
    <message>
        <source>Mouse Motion Acceleration</source>
        <translation type="vanished">鼠标移动加速</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">是否为自然滚动</translation>
    </message>
    <message>
        <source>Middle Emulation Enabled</source>
        <translation type="vanished">同时按下左右键模拟中键</translation>
    </message>
    <message>
        <source>Touchpad Enabled</source>
        <translation type="vanished">禁用触摸板</translation>
    </message>
    <message>
        <source>Select TouchPad Hand</source>
        <translation type="vanished">选择触摸板使用模式</translation>
    </message>
    <message>
        <source>TouchPad Motion Acceleration</source>
        <translation type="vanished">触摸板移动加速</translation>
    </message>
    <message>
        <source>Select Click Method</source>
        <translation type="vanished">设置点击触摸板方式</translation>
    </message>
    <message>
        <source>Select Scroll Method</source>
        <translation type="vanished">滚动窗口方式</translation>
    </message>
    <message>
        <source>Enabled while Typing</source>
        <translation type="vanished">打字时触摸板禁用</translation>
    </message>
    <message>
        <source>Tap to Click</source>
        <translation type="vanished">轻击(不按下)触摸板功能是否生效</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">重置</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">保存</translation>
    </message>
    <message>
        <source>Mouse Settings</source>
        <translation type="vanished">鼠标设置</translation>
    </message>
    <message>
        <source>TouchPad Settings</source>
        <translation type="vanished">触摸板设置</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">标准</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">右手模式</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">左手模式</translation>
    </message>
    <message>
        <source>Press and Tap</source>
        <translation type="vanished">按键和轻触</translation>
    </message>
    <message>
        <source>Tap</source>
        <translation type="vanished">轻触</translation>
    </message>
    <message>
        <source>Two Finger Scroll</source>
        <translation type="vanished">两指滑动</translation>
    </message>
    <message>
        <source>Edge Scroll</source>
        <translation type="vanished">边缘滑动</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">低速</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">快速</translation>
    </message>
</context>
<context>
    <name>MousePage</name>
    <message>
        <location filename="../src/mouse-page.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="68"/>
        <source>Select Mouse Hand</source>
        <translation>选择鼠标手持模式</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="103"/>
        <source>ComboSelectMouseHand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="126"/>
        <source>Mouse Motion Acceleration</source>
        <translation>鼠标移动加速</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="133"/>
        <source>SliderMouseMotionAcceleration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="163"/>
        <source>Slow</source>
        <translation>慢</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="183"/>
        <source>Fast</source>
        <translation>快</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="216"/>
        <source>Natural Scroll</source>
        <translation>是否为自然滚动</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="236"/>
        <source>SwitchMouseNatturalScroll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="269"/>
        <source>Middle Emulation Enabled</source>
        <translation>同时按下左右键模拟中键</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="289"/>
        <source>SwitchMiddleEmulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="335"/>
        <source>Test mouse wheel direction</source>
        <translation>鼠标滚轮方向测试</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.ui" line="372"/>
        <source>This is line 1 of the test text
This is line 2 of the test text
This is line 3 of the test text
This is line 4 of the test text
This is line 5 of the test text
This is line 6 of the test text
This is line 7 of the test text
This is line 8 of the test text
This is line 9 of the test text
This is line 10 of the test text
This is line 11 of the test text
This is line 12 of the test text
This is line 13 of the test text
This is line 14 of the test text
This is line 15 of the test text
This is line 16 of the test text
This is line 17 of the test text
This is line 18 of the test text
This is line 19 of the test text
This is line 20 of the test text
This is line 21 of the test text
This is line 22 of the test text
This is line 23 of the test text
This is line 24 of the test text
This is line 25 of the test text
This is line 26 of the test text
This is line 27 of the test text
This is line 28 of the test text
This is line 29 of the test text
This is line 30 of the test text
This is line 31 of the test text
This is line 32 of the test text
This is line 33 of the test text
This is line 34 of the test text
This is line 35 of the test text
This is line 36 of the test text
This is line 37 of the test text
This is line 38 of the test text
This is line 39 of the test text
This is line 40 of the test text
This is line 41 of the test text
This is line 42 of the test text
This is line 43 of the test text
This is line 44 of the test text
This is line 45 of the test text
This is line 46 of the test text
This is line 47 of the test text
This is line 48 of the test text
This is line 49 of the test text
This is line 50 of the test text</source>
        <translation>这是第1行测试文字
这是第2行测试文字
这是第3行测试文字
这是第4行测试文字
这是第5行测试文字
这是第6行测试文字
这是第7行测试文字
这是第8行测试文字
这是第9行测试文字
这是第10行测试文字
这是第11行测试文字
这是第12行测试文字
这是第13行测试文字
这是第14行测试文字
这是第15行测试文字
这是第16行测试文字
这是第17行测试文字
这是第18行测试文字
这是第19行测试文字
这是第20行测试文字
这是第21行测试文字
这是第22行测试文字
这是第23行测试文字
这是第24行测试文字
这是第25行测试文字
这是第26行测试文字
这是第27行测试文字
这是第28行测试文字
这是第29行测试文字
这是第30行测试文字
这是第31行测试文字
这是第32行测试文字
这是第33行测试文字
这是第34行测试文字
这是第35行测试文字
这是第36行测试文字
这是第37行测试文字
这是第38行测试文字
这是第39行测试文字
这是第40行测试文字
这是第41行测试文字
这是第42行测试文字
这是第43行测试文字
这是第44行测试文字
这是第45行测试文字
这是第46行测试文字
这是第47行测试文字
这是第48行测试文字
这是第49行测试文字
这是第50行测试文字</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.cpp" line="81"/>
        <source>Right Hand Mode</source>
        <translation>右手模式</translation>
    </message>
    <message>
        <location filename="../src/mouse-page.cpp" line="81"/>
        <source>Left Hand Mode</source>
        <translation>左手模式</translation>
    </message>
</context>
<context>
    <name>MouseSettings</name>
    <message>
        <source>Select Mouse Hand</source>
        <translation type="vanished">选择鼠标手持模式</translation>
    </message>
    <message>
        <source>Mouse Motion Acceleration</source>
        <translation type="vanished">鼠标移动加速</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">是否为自然滚动</translation>
    </message>
    <message>
        <source>Middle Emulation Enabled</source>
        <translation type="vanished">同时按下左右键模拟中键</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">右手模式</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">左手模式</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">慢</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">标准</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">快</translation>
    </message>
</context>
<context>
    <name>MouseSubItem</name>
    <message>
        <location filename="../src/mouse-subitem.h" line="46"/>
        <source>Mouse Settings</source>
        <translation>鼠标设置</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>SLow</source>
        <translation type="vanished">低速</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">标准</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">快速</translation>
    </message>
    <message>
        <source>Faild</source>
        <translation type="vanished">失败</translation>
    </message>
    <message>
        <source>Connect Mouse or TouchPad Dbus Failed!</source>
        <translation type="vanished">连接鼠标或触摸板Dbus服务失败！</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>Load qss file failed!</source>
        <translation type="vanished">加载qss文件失败!</translation>
    </message>
</context>
<context>
    <name>TouchPadPage</name>
    <message>
        <location filename="../src/touchpad-page.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="86"/>
        <source>TouchPad Enabled</source>
        <translation>开启触摸板</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="106"/>
        <source>SwitchTouchPadEnable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="137"/>
        <source>Select TouchPad Hand</source>
        <translation>选择触摸板使用模式</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="156"/>
        <source>ComboTouchPadHand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="189"/>
        <source>TouchPad Motion Acceleration</source>
        <translation>触摸板移动加速</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="211"/>
        <source>SliderTouchPadMotionAcceleration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="235"/>
        <source>Slow</source>
        <translation>慢</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="255"/>
        <source>Fast</source>
        <translation>快</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="285"/>
        <source>Select Click Method</source>
        <translation>设置点击触摸板方式</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="304"/>
        <source>ComboClickMethod</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="332"/>
        <source>Select Scroll Method</source>
        <translation>滚动窗口方式</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="351"/>
        <source>ComboScrollMethod</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="379"/>
        <source>Natural Scroll</source>
        <translation>是否为自然滚动</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="399"/>
        <source>ComboNaturalScroll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="430"/>
        <source>Enabled while Typing</source>
        <translation>打字时触摸板禁用</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="450"/>
        <source>SwitchTypingEnable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="481"/>
        <source>Tap to Click</source>
        <translation>轻击(不按下)触摸板功能是否生效</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.ui" line="501"/>
        <source>SwtichTapToClick</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="268"/>
        <source>Right Hand Mode</source>
        <translation>右手模式</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="268"/>
        <source>Left Hand Mode</source>
        <translation>左手模式</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="272"/>
        <source>Press and Tap</source>
        <translation>按键和轻触</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="272"/>
        <source>Tap</source>
        <translation>轻触</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="276"/>
        <source>Two Finger Scroll</source>
        <translation>两指滑动</translation>
    </message>
    <message>
        <location filename="../src/touchpad-page.cpp" line="276"/>
        <source>Edge Scroll</source>
        <translation>边缘滑动</translation>
    </message>
</context>
<context>
    <name>TouchPadSettings</name>
    <message>
        <source>Touchpad Enabled</source>
        <translation type="vanished">禁用触摸板</translation>
    </message>
    <message>
        <source>Disable TouchPad</source>
        <translation type="vanished">禁用触摸板</translation>
    </message>
    <message>
        <source>TouchPad Enabled</source>
        <translation type="vanished">开启触摸板</translation>
    </message>
    <message>
        <source>Select TouchPad Hand</source>
        <translation type="vanished">选择触摸板使用模式</translation>
    </message>
    <message>
        <source>TouchPad Motion Acceleration</source>
        <translation type="vanished">触摸板移动加速</translation>
    </message>
    <message>
        <source>Select Click Method</source>
        <translation type="vanished">设置点击触摸板方式</translation>
    </message>
    <message>
        <source>Select Scroll Method</source>
        <translation type="vanished">滚动窗口方式</translation>
    </message>
    <message>
        <source>Natural Scroll</source>
        <translation type="vanished">是否为自然滚动</translation>
    </message>
    <message>
        <source>Enabled while Typing</source>
        <translation type="vanished">打字时触摸板禁用</translation>
    </message>
    <message>
        <source>Tap to Click</source>
        <translation type="vanished">轻击(不按下)触摸板功能是否生效</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">慢</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">标准</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">快</translation>
    </message>
    <message>
        <source>Right Hand Mode</source>
        <translation type="vanished">右手模式</translation>
    </message>
    <message>
        <source>Left Hand Mode</source>
        <translation type="vanished">左手模式</translation>
    </message>
    <message>
        <source>Press and Tap</source>
        <translation type="vanished">按键和轻触</translation>
    </message>
    <message>
        <source>Tap</source>
        <translation type="vanished">轻触</translation>
    </message>
    <message>
        <source>Two Finger Scroll</source>
        <translation type="vanished">两指滑动</translation>
    </message>
    <message>
        <source>Edge Scroll</source>
        <translation type="vanished">边缘滑动</translation>
    </message>
</context>
<context>
    <name>TouchPadSubItem</name>
    <message>
        <location filename="../src/touchpad-subitem.h" line="46"/>
        <source>TouchPad Settings</source>
        <translation>触摸板设置</translation>
    </message>
</context>
</TS>
