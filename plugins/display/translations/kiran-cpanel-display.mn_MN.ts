<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>DevicePanel</name>
    <message>
        <location filename="../src/device-panel.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Маягт</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="122"/>
        <source>Rotate left 90 degrees</source>
        <translation>Эргэдэг зүүн 90 градус</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="125"/>
        <source>ButtonLeft</source>
        <translation type="unfinished">ButtonLeft</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="156"/>
        <source>Rotate right 90 degrees</source>
        <translation>Зөв 90 градус</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="159"/>
        <source>ButtonRight</source>
        <translation type="unfinished">Товч</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="190"/>
        <source>Turn left and right</source>
        <translation>Зүүн ба баруун тийш эргэ</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="193"/>
        <source>ButtonHorizontal</source>
        <translation type="unfinished">ButtonHorizontal</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="227"/>
        <source>upside down</source>
        <translation>доошоо</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="230"/>
        <source>ButtonVertical</source>
        <translation type="unfinished">Товч</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="264"/>
        <source>Identification display</source>
        <translation>Таних дэлгэц</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="267"/>
        <source>ButtonIdentifying</source>
        <translation type="unfinished">ButtonIdent</translation>
    </message>
</context>
<context>
    <name>DisplayPage</name>
    <message>
        <location filename="../src/display-page.ui" line="14"/>
        <source>DisplayPage</source>
        <translation type="unfinished">Дэлгэцийн</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="110"/>
        <source>ButtonCopyDisplay</source>
        <translation type="unfinished">ButtonCopyDisplay</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="134"/>
        <source>Copy display</source>
        <translation>Дэлгэц</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="162"/>
        <source>ButtonExtendedDisplay</source>
        <translation type="unfinished">ButtonExtendedDisplay</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="186"/>
        <source>Extended display</source>
        <translation>Өргөтгөсөн дэлгэц</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="289"/>
        <location filename="../src/display-page.ui" line="562"/>
        <source>Resolution ratio</source>
        <translation>Тогтоолын харьцаа</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="308"/>
        <source>ComboResolutionRatio</source>
        <translation type="unfinished">ComboResolutionRatio</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="325"/>
        <location filename="../src/display-page.ui" line="598"/>
        <source>Refresh rate</source>
        <translation>Шинэчлэх</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="344"/>
        <source>ComboRefreshRate</source>
        <translation type="unfinished">ComboRefreshRate</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="358"/>
        <location filename="../src/display-page.ui" line="631"/>
        <source>Zoom rate</source>
        <translation>Томруулах</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="377"/>
        <source>ComboZoomRate</source>
        <translation type="unfinished">ComboZoomRate</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="381"/>
        <location filename="../src/display-page.ui" line="654"/>
        <source>Automatic</source>
        <translation>Автомат</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="386"/>
        <location filename="../src/display-page.ui" line="659"/>
        <source>100% (recommended)</source>
        <translation>100% ( санал болгосон )</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="391"/>
        <location filename="../src/display-page.ui" line="664"/>
        <source>200%</source>
        <translation type="unfinished">200%</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="462"/>
        <source>Open</source>
        <translation>Нээлттэй</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="514"/>
        <source>Set as main display</source>
        <translation>Үндсэн дэлгэц болгон тохируулна</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="540"/>
        <source>SwitchExtraPrimary</source>
        <translation type="unfinished">SwitchExtraPrimary</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="581"/>
        <source>ComboExtraResolutionRatio</source>
        <translation type="unfinished">ComboExtraResolutionRatio</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="617"/>
        <source>ComboExtraRefreshRate</source>
        <translation type="unfinished">ComboExtraRefreshRate</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="650"/>
        <source>ComboExtraZoomRate</source>
        <translation type="unfinished">ComboExtraZoomRate</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="731"/>
        <source>ButtonExtraApply</source>
        <translation type="unfinished">ButtonExtraApply</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="737"/>
        <source>Apply</source>
        <translation>Хэрэглэх</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="756"/>
        <source>ButtonExtraCancel</source>
        <translation type="unfinished">ButtonExtraCancel</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="762"/>
        <source>Close</source>
        <translation>Хаах</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="255"/>
        <location filename="../src/display-page.cpp" line="279"/>
        <source> (recommended)</source>
        <translation>( санал болгосон )</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="351"/>
        <source>Is the display normal?</source>
        <translation>Дэлгэц хэвийн үү?</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="354"/>
        <source>Save current configuration(K)</source>
        <translation>Одоогийн тохиргоог хадгалах ( K )</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="358"/>
        <source>Restore previous configuration(R)</source>
        <translation>Өмнөх тохиргоог сэргээх ( R )</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="366"/>
        <source>The display will resume the previous configuration in %1 seconds</source>
        <translation>Дэлгэц нь өмнөх тохиргоог% 1 секундын дотор дахин эхлүүлэх болно</translation>
    </message>
</context>
<context>
    <name>DisplaySubitem</name>
    <message>
        <location filename="../src/display-subitem.h" line="29"/>
        <source>Display</source>
        <translation>Дэлгэц</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/display-config.cpp" line="466"/>
        <location filename="../src/display-config.h" line="134"/>
        <location filename="../src/display-page.cpp" line="387"/>
        <location filename="../src/display-page.cpp" line="405"/>
        <source>Tips</source>
        <translation>Зөвлөмж</translation>
    </message>
    <message>
        <location filename="../src/display-config.cpp" line="469"/>
        <location filename="../src/display-config.h" line="137"/>
        <location filename="../src/display-page.cpp" line="390"/>
        <location filename="../src/display-page.cpp" line="408"/>
        <source>OK(K)</source>
        <translation>OK ( K )</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="394"/>
        <source>Failed to apply display settings!%1</source>
        <translation>Дэлгэцийн тохиргоог ашиглаж чадсангүй!%1</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="412"/>
        <source>Fallback display setting failed! %1</source>
        <translation>Fallback дэлгэцийн тохиргоо амжилтгүй боллоо! %1</translation>
    </message>
</context>
</TS>
