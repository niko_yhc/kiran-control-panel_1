<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>DevicePanel</name>
    <message>
        <location filename="../src/device-panel.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">شەكىل</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="122"/>
        <source>Rotate left 90 degrees</source>
        <translation>سولغا 90 گىرادۇس ئايلىنىش</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="125"/>
        <source>ButtonLeft</source>
        <translation type="unfinished">كۇنۇپكىسى سول تەرەپ</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="156"/>
        <source>Rotate right 90 degrees</source>
        <translation>ئوڭغا 90 گىرادۇس ئايلىنىش</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="159"/>
        <source>ButtonRight</source>
        <translation type="unfinished">ئوڭ كۇنۇپكا</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="190"/>
        <source>Turn left and right</source>
        <translation>سولغا بۇرۇلۇڭ ئوڭغا بۇرۇلۇڭ</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="193"/>
        <source>ButtonHorizontal</source>
        <translation type="unfinished">بۇدۇن. گورىزونتال</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="227"/>
        <source>upside down</source>
        <translation>ئاستىن-ئۈستۈن قىلىۋەتمەك</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="230"/>
        <source>ButtonVertical</source>
        <translation type="unfinished">كۇنۇپكىسى تىك</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="264"/>
        <source>Identification display</source>
        <translation>پەرقلەندۈرۈش كۆرسەتكۈچى</translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="267"/>
        <source>ButtonIdentifying</source>
        <translation type="unfinished">كۇنۇپكىلىق پەرقلەندۈرۈش</translation>
    </message>
</context>
<context>
    <name>DisplayPage</name>
    <message>
        <location filename="../src/display-page.ui" line="14"/>
        <source>DisplayPage</source>
        <translation type="unfinished">ISplayage</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="110"/>
        <source>ButtonCopyDisplay</source>
        <translation type="unfinished">Uttonopyisplay</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="134"/>
        <source>Copy display</source>
        <translation>كۆپەيتىپ كۆرسىتىش</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="162"/>
        <source>ButtonExtendedDisplay</source>
        <translation type="unfinished">كۇنۇپكىلىق كېڭەيتىپ كۆرسىتىش</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="186"/>
        <source>Extended display</source>
        <translation>كېڭەيتىپ كۆرسىتىش</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="289"/>
        <location filename="../src/display-page.ui" line="562"/>
        <source>Resolution ratio</source>
        <translation>پەرقلەندۈرۈش نىسبىتى</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="308"/>
        <source>ComboResolutionRatio</source>
        <translation type="unfinished">بىرىكمە ئانالىتىك نىسبەت</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="325"/>
        <location filename="../src/display-page.ui" line="598"/>
        <source>Refresh rate</source>
        <translation>يېڭىلاش نىسبىتى</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="344"/>
        <source>ComboRefreshRate</source>
        <translation type="unfinished">بىرىكمە يېڭىلاش نىسبىتى</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="358"/>
        <location filename="../src/display-page.ui" line="631"/>
        <source>Zoom rate</source>
        <translation>فوكۇس ئۆزگەرتىش تېزلىكى</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="377"/>
        <source>ComboZoomRate</source>
        <translation type="unfinished">بىرىكمە كىچىكلىتىش نىسبىتى</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="381"/>
        <location filename="../src/display-page.ui" line="654"/>
        <source>Automatic</source>
        <translation>ئاپتوماتىك</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="386"/>
        <location filename="../src/display-page.ui" line="659"/>
        <source>100% (recommended)</source>
        <translation>%100 ( تەكلىپ )</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="391"/>
        <location filename="../src/display-page.ui" line="664"/>
        <source>200%</source>
        <translation type="unfinished">200%</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="462"/>
        <source>Open</source>
        <translation>ئاچماق</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="514"/>
        <source>Set as main display</source>
        <translation>تەسىس قىلىش ئاساس قىلىنغان كۆرسىتىش</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="540"/>
        <source>SwitchExtraPrimary</source>
        <translation type="unfinished">بۆرەك ئۈستى بېزى سىرتقى ۋىكليۇچاتېلى</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="581"/>
        <source>ComboExtraResolutionRatio</source>
        <translation type="unfinished">بىرىكمە سىرتقى پەرق ئېتىش نىسبىتى</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="617"/>
        <source>ComboExtraRefreshRate</source>
        <translation type="unfinished">ComboExtraRefreshRate</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="650"/>
        <source>ComboExtraZoomRate</source>
        <translation type="unfinished">ئازوسىرتقى ئېستېر</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="731"/>
        <source>ButtonExtraApply</source>
        <translation type="unfinished">T شەكىللىك ئىشتان سىقىپ بوتقىلاش</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="737"/>
        <source>Apply</source>
        <translation>قوللىنىش</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="756"/>
        <source>ButtonExtraCancel</source>
        <translation type="unfinished">تۈگمە سىرتقى راكى</translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="762"/>
        <source>Close</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="255"/>
        <location filename="../src/display-page.cpp" line="279"/>
        <source> (recommended)</source>
        <translation>( تەكلىپ )</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="351"/>
        <source>Is the display normal?</source>
        <translation>نورمال كۆرۈندىمۇ؟</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="354"/>
        <source>Save current configuration(K)</source>
        <translation>نۆۋەتتىكى سەپلىمىنى ساقلاش ( K )</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="358"/>
        <source>Restore previous configuration(R)</source>
        <translation>ئوكسىدسىزلىنىشتىن ئىلگىرىكى سەپلەش ( R )</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="366"/>
        <source>The display will resume the previous configuration in %1 seconds</source>
        <translation>كۆرسەتكۈچ كېلەر % 1 كېيىن ئەسلىگە كەلتۈرۈش بۇرۇنقى تەقسىملەش</translation>
    </message>
</context>
<context>
    <name>DisplaySubitem</name>
    <message>
        <location filename="../src/display-subitem.h" line="29"/>
        <source>Display</source>
        <translation>كۆرسىتىش</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/display-config.cpp" line="466"/>
        <location filename="../src/display-config.h" line="134"/>
        <location filename="../src/display-page.cpp" line="387"/>
        <location filename="../src/display-page.cpp" line="405"/>
        <source>Tips</source>
        <translation>ئەسكەرتىش</translation>
    </message>
    <message>
        <location filename="../src/display-config.cpp" line="469"/>
        <location filename="../src/display-config.h" line="137"/>
        <location filename="../src/display-page.cpp" line="390"/>
        <location filename="../src/display-page.cpp" line="408"/>
        <source>OK(K)</source>
        <translation>بولىدۇ ( K )</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="394"/>
        <source>Failed to apply display settings!%1</source>
        <translation>قوللىنىشچان كۆرسىتىش تەسىس قىلىش مەغلۇپ بولدى ! %1</translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="412"/>
        <source>Fallback display setting failed! %1</source>
        <translation>قايتۇرۇش كۆرسىتىش تەسىس قىلىش مەغلۇپ بولدى ! %1</translation>
    </message>
</context>
</TS>
