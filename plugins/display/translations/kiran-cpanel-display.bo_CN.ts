<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>DevicePanel</name>
    <message>
        <location filename="../src/device-panel.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">རྣམ་པ། </translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="122"/>
        <source>Rotate left 90 degrees</source>
        <translation>གཡོན་དུ་འཁོར་སྐྱོད་ཏུའུ་90། </translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="125"/>
        <source>ButtonLeft</source>
        <translation type="unfinished">གཅུས་སྒོ་གཡོན། </translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="156"/>
        <source>Rotate right 90 degrees</source>
        <translation>གཡས་འཁོར་སྐྱོད་ཏུའུ་90། </translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="159"/>
        <source>ButtonRight</source>
        <translation type="unfinished">གཅུས་སྒོ་གཡས། </translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="190"/>
        <source>Turn left and right</source>
        <translation>གཡོན་དུ་བསྒྱུར་བ་དང་གཡས་བསྐོར། </translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="193"/>
        <source>ButtonHorizontal</source>
        <translation type="unfinished">གཅུས་སྒོ་ཆུ་ཚད། </translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="227"/>
        <source>upside down</source>
        <translation>གོ་ལྡོག། </translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="230"/>
        <source>ButtonVertical</source>
        <translation type="unfinished">གཅུས་སྒོ་དྲང་འཕྱང། </translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="264"/>
        <source>Identification display</source>
        <translation>ངོས་འཛིན་མངོན་པ། </translation>
    </message>
    <message>
        <location filename="../src/device-panel.ui" line="267"/>
        <source>ButtonIdentifying</source>
        <translation type="unfinished">གཅུས་སྒོ་ངོས་འཛིན། </translation>
    </message>
</context>
<context>
    <name>DisplayPage</name>
    <message>
        <location filename="../src/display-page.ui" line="14"/>
        <source>DisplayPage</source>
        <translation type="unfinished">མངོན་པའི་ཤོག་ལྷེ། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="110"/>
        <source>ButtonCopyDisplay</source>
        <translation type="unfinished">གཅུས་སྒོ་འདྲ་བཟོ་མངོན་པ། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="134"/>
        <source>Copy display</source>
        <translation>འདྲ་བཟོ་མངོན་པ། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="162"/>
        <source>ButtonExtendedDisplay</source>
        <translation type="unfinished">གཅུས་སྒོ་བསྐྱེད་པའི་ཐོག་ནས་མངོན་པ་ར། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="186"/>
        <source>Extended display</source>
        <translation>བསྐྱེད་པའི་ཐོག་ནས་མངོན་པ་ར། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="289"/>
        <location filename="../src/display-page.ui" line="562"/>
        <source>Resolution ratio</source>
        <translation>分辨率། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="308"/>
        <source>ComboResolutionRatio</source>
        <translation type="unfinished">སྡེབ་སྒྲིག་分辨率། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="325"/>
        <location filename="../src/display-page.ui" line="598"/>
        <source>Refresh rate</source>
        <translation>གསར་བཅོས་ཚད། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="344"/>
        <source>ComboRefreshRate</source>
        <translation type="unfinished">སྡེབ་སྒྲིག་གསར་བཅོས་ཚད། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="358"/>
        <location filename="../src/display-page.ui" line="631"/>
        <source>Zoom rate</source>
        <translation>འགྱུར་འཚིག་པ་ཚད། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="377"/>
        <source>ComboZoomRate</source>
        <translation type="unfinished">སྡེབ་སྒྲིག་缩放ཚད། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="381"/>
        <location filename="../src/display-page.ui" line="654"/>
        <source>Automatic</source>
        <translation>རང་འགུལ། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="386"/>
        <location filename="../src/display-page.ui" line="659"/>
        <source>100% (recommended)</source>
        <translation>100%（འོས་སྦྱོར་）། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="391"/>
        <location filename="../src/display-page.ui" line="664"/>
        <source>200%</source>
        <translation type="unfinished">200% </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="462"/>
        <source>Open</source>
        <translation>ཕྱེ། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="514"/>
        <source>Set as main display</source>
        <translation>རྒྱུའི་གཙོ་བོ་བརྡ་སྟོན་ཡོ་ཆས། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="540"/>
        <source>SwitchExtraPrimary</source>
        <translation type="unfinished">ཁ་པར་མཐུད་ཆས་འཕར་མ་གཙོ། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="581"/>
        <source>ComboExtraResolutionRatio</source>
        <translation type="unfinished">སྡེབ་སྒྲིག་འཕར་མ་分辨率། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="617"/>
        <source>ComboExtraRefreshRate</source>
        <translation type="unfinished">སྡེབ་སྒྲིག་འཕར་མ་གསར་བཅོས་ཚད། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="650"/>
        <source>ComboExtraZoomRate</source>
        <translation type="unfinished">སྡེབ་སྒྲིག་འཕར་མ་缩放ཚད། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="731"/>
        <source>ButtonExtraApply</source>
        <translation type="unfinished">གཅུས་སྒོ་འཕར་མ་ཉེར་སྤྱོད། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="737"/>
        <source>Apply</source>
        <translation>ཉེར་སྤྱོད། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="756"/>
        <source>ButtonExtraCancel</source>
        <translation type="unfinished">གཅུས་སྒོ་འཕར་མ་བཟོས་པ། </translation>
    </message>
    <message>
        <location filename="../src/display-page.ui" line="762"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག། </translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="255"/>
        <location filename="../src/display-page.cpp" line="279"/>
        <source> (recommended)</source>
        <translation>（འོས་སྦྱོར་）། </translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="351"/>
        <source>Is the display normal?</source>
        <translation>མངོན་པའི་རྒྱུན་ལྡན་གྱི་ཡིན་ནམ། </translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="354"/>
        <source>Save current configuration(K)</source>
        <translation>ཉར་འཛིན་དེང་སྐབས་བཀོད་སྒྲིག་(K）། </translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="358"/>
        <source>Restore previous configuration(R)</source>
        <translation>སླར་གསོ་སྔོན་ལ་བཀོད་སྒྲིག་（R）། </translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="366"/>
        <source>The display will resume the previous configuration in %1 seconds</source>
        <translation>བརྙན་ཤེལ་ས་%1སྐར་ཆ་འི་རྗེས་ཀྱི་སླར་གསོ་འི་སྔོན་ལ་བཀོད་སྒྲིག། </translation>
    </message>
</context>
<context>
    <name>DisplaySubitem</name>
    <message>
        <location filename="../src/display-subitem.h" line="29"/>
        <source>Display</source>
        <translation>མངོན་པ། </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/display-config.cpp" line="466"/>
        <location filename="../src/display-config.h" line="134"/>
        <location filename="../src/display-page.cpp" line="387"/>
        <location filename="../src/display-page.cpp" line="405"/>
        <source>Tips</source>
        <translation>ཐབས་རྩལ། </translation>
    </message>
    <message>
        <location filename="../src/display-config.cpp" line="469"/>
        <location filename="../src/display-config.h" line="137"/>
        <location filename="../src/display-page.cpp" line="390"/>
        <location filename="../src/display-page.cpp" line="408"/>
        <source>OK(K)</source>
        <translation>ལེགས་པའི་(K）། </translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="394"/>
        <source>Failed to apply display settings!%1</source>
        <translation>ཐབས་མེད་པར་ཉེར་སྤྱོད་མཉེན་ཆས་བཙུགས། %1 </translation>
    </message>
    <message>
        <location filename="../src/display-page.cpp" line="412"/>
        <source>Fallback display setting failed! %1</source>
        <translation>ཕྱིར་ནུར་མངོན་པའི་སྒྲིག་བཀོད་དང་ཕམ་ཁ། %1 </translation>
    </message>
</context>
</TS>
