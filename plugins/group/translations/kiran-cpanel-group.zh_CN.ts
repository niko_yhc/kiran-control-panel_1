<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>CreateGroupPage</name>
    <message>
        <location filename="../src/pages/create-group-page/create-group-page.ui" line="32"/>
        <source>CreateGroupPage</source>
        <translation>创建用户组</translation>
    </message>
    <message>
        <location filename="../src/pages/create-group-page/create-group-page.ui" line="95"/>
        <source>Create Group</source>
        <translation>创建组</translation>
    </message>
    <message>
        <location filename="../src/pages/create-group-page/create-group-page.ui" line="166"/>
        <source>Add Group Members</source>
        <translation>添加组成员</translation>
    </message>
    <message>
        <location filename="../src/pages/create-group-page/create-group-page.ui" line="246"/>
        <source>Confirm</source>
        <translation>创建</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">返回</translation>
    </message>
    <message>
        <location filename="../src/pages/create-group-page/create-group-page.cpp" line="101"/>
        <source>Please enter your group name</source>
        <translation>请输入组名</translation>
    </message>
    <message>
        <location filename="../src/pages/create-group-page/create-group-page.cpp" line="118"/>
        <source>group name cannot be a pure number</source>
        <translation>组名不能为纯数字</translation>
    </message>
    <message>
        <location filename="../src/pages/create-group-page/create-group-page.cpp" line="125"/>
        <source>group name already exists</source>
        <translation>组名已经存在</translation>
    </message>
    <message>
        <location filename="../src/pages/create-group-page/create-group-page.cpp" line="153"/>
        <location filename="../src/pages/create-group-page/create-group-page.cpp" line="172"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
</context>
<context>
    <name>GroupInfoPage</name>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.ui" line="32"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.ui" line="136"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.ui" line="233"/>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.ui" line="332"/>
        <source>Member List</source>
        <translation>组成员</translation>
    </message>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.ui" line="432"/>
        <source>Add User</source>
        <translation>添加成员</translation>
    </message>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.ui" line="676"/>
        <source>Save</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.ui" line="723"/>
        <source>Cancel</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.ui" line="561"/>
        <source>Add Member</source>
        <translation>添加组成员</translation>
    </message>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.ui" line="479"/>
        <source>Delete</source>
        <translation>删除组</translation>
    </message>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.cpp" line="123"/>
        <source>Please input keys for search...</source>
        <translation>请输入搜索关键词...</translation>
    </message>
    <message>
        <location filename="../src/pages/group-info-page/group-info-page.cpp" line="247"/>
        <location filename="../src/pages/group-info-page/group-info-page.cpp" line="259"/>
        <location filename="../src/pages/group-info-page/group-info-page.cpp" line="269"/>
        <location filename="../src/pages/group-info-page/group-info-page.cpp" line="280"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
</context>
<context>
    <name>GroupSubItem</name>
    <message>
        <location filename="../src/group-subitem.cpp" line="44"/>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <location filename="../src/group-subitem.cpp" line="76"/>
        <source>Chnage time Zone</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/group-subitem.cpp" line="77"/>
        <source>Set time Manually</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/group-subitem.cpp" line="78"/>
        <source>Time date format setting</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>HardWorker</name>
    <message>
        <location filename="../src/hard-worker.cpp" line="38"/>
        <source>Create Group failed</source>
        <translation>创建组失败</translation>
    </message>
    <message>
        <location filename="../src/hard-worker.cpp" line="92"/>
        <source>Failed to delete group,%1</source>
        <translation>删除组失败，%1</translation>
    </message>
    <message>
        <location filename="../src/hard-worker.cpp" line="113"/>
        <location filename="../src/hard-worker.cpp" line="135"/>
        <source> add user to group failed</source>
        <translation>添加用户到组失败</translation>
    </message>
    <message>
        <location filename="../src/hard-worker.cpp" line="158"/>
        <source> change group name failed</source>
        <translation>修改组名失败</translation>
    </message>
    <message>
        <location filename="../src/hard-worker.cpp" line="169"/>
        <source> change group name failed, the new group name is occupied</source>
        <translation>修改组名失败，新组名已存在</translation>
    </message>
</context>
<context>
    <name>KiranGroupManager</name>
    <message>
        <location filename="../src/kiran-group-manager.cpp" line="146"/>
        <source>Create new group</source>
        <translation>创建组</translation>
    </message>
</context>
<context>
    <name>KiranTips</name>
    <message>
        <location filename="../src/widgets/kiran-tips.ui" line="29"/>
        <source>Form</source>
        <translation ></translation>
    </message>
</context>
</TS>
