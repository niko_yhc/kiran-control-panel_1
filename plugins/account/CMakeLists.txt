set(TARGET_NAME kiran-cpanel-account)

if (PASSWD_EXPIRATION_POLICY_VISIBLE)
    add_definitions(-DPASSWD_EXPIRATION_POLICY)
endif ()

pkg_search_module(CRYPTOPP REQUIRED cryptopp)
pkg_search_module(PAM REQUIRED pam)
pkg_search_module(LIBCRYPT REQUIRED libcrypt)

file(GLOB_RECURSE ACCOUNT_SRC
        ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/*.h
        ${CMAKE_CURRENT_SOURCE_DIR}/pages/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/pages/*.h
        ${CMAKE_CURRENT_SOURCE_DIR}/pages/*.ui
        ${CMAKE_CURRENT_SOURCE_DIR}/utils/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/utils/*.h
        ${CMAKE_CURRENT_SOURCE_DIR}/utils/*.ui)

add_library(${TARGET_NAME} SHARED ${ACCOUNT_SRC})

target_include_directories(${TARGET_NAME} PRIVATE
        ${CMAKE_BINARY_DIR}
        ${CMAKE_CURRENT_SOURCE_DIR}/pages
        ${CMAKE_CURRENT_SOURCE_DIR}/utils
	${PAM_INCLUDE_DIRS}
        ${KIRAN_WIDGETS_INCLUDE_DIRS}
        ${KIRAN_CC_DAEMON_INCLUDE_DIRS}
        ${KLOG_INCLUDE_DIRS}
        ${KIRAN_STYLE_INCLUDE_DIRS}
        ${CRYPTOPP_INCLUDE_DIRS}
	${LIBCRYPT_INCLUDE_DIRS})

target_link_libraries(${TARGET_NAME}
        common-widgets
        dbus-wrapper
        Qt5::Widgets
        Qt5::DBus
        Qt5::Svg
	${PAM_LIBRARIES}
        ${KIRAN_WIDGETS_LIBRARIES}
        ${KIRAN_CC_DAEMON_LIBRARIES}
        ${KLOG_LIBRARIES}
        ${KIRAN_STYLE_LIBRARIES}
        ${CRYPTOPP_LIBRARIES}
	${LIBCRYPT_LIBRARIES})

install(TARGETS ${TARGET_NAME} DESTINATION ${PLUGIN_LIBS_DIR}/)
