/**
 * Copyright (c) 2020 ~ 2021 KylinSec Co., Ltd.
 * kiran-control-panel is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 *
 * Author:     liuxinhao <liuxinhao@kylinsec.com.cn>
 */

#ifndef KIRAN_ACCOUNT_MANAGER_UI_DEFINES_H
#define KIRAN_ACCOUNT_MANAGER_UI_DEFINES_H

#define CUTTING_CIRCLE_RADIUS 150

#define IMAGE_SCALE_FACTOR_MAXIMUM 100
#define IMAGE_SCALE_FACTOR_MINIMUM (1 / 2.0)

#define SCALE_STEP_VALUE 0.05

#endif  //KIRAN_ACCOUNT_MANAGER_UI_DEFINES_H
